package wdk.gui;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import static wdk.WDK_INITConstants.PATH_CSS;
import wdk.WDK_PropertyType;
import wdk.controller.DraftEditController;
import wdk.controller.FantasyTeamController;
import wdk.controller.FileController;
import wdk.controller.PlayerController;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.data.FantasyTeam;
import wdk.file.ImageLoader;

public class FantasyTeamsScreen {

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_TEXT_LABEL = "text_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_TOGGLE_GROUP = "toggle_group";
    static final String CLASS_HEADING_BOX = "heading_box";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    MessageDialog messageDialog;
    FileController fileController;
    FantasyTeamController fantasyController;
    Stage primaryStage;
    Draft draft;
    DraftDataManager dataManager;
    BorderPane fantasyTeamsPane;
    BorderPane lineupPane;
    BorderPane taxiSquadPane;
    Label fantasyTeamLabel;
    FilteredList<Player> filteredPlayers;
    //SortedList<Player> sortedLineup;
    SortedList<Player> sortedTaxi;
    INITComponents componentMaker;
    PlayerController playerController;
    ImageLoader imageLoader;
    DraftEditController draftController;

    Player currentPlayer;

    ObservableList<Player> currentTeamRoster;

    TableView<Player> startingLineupTable;
    TableView<Player> taxiSquadTable;

    ObservableList<FantasyTeam> teams;
    FantasyTeam dummyTeam;

    ObservableList<Player> currentLineup;
    ObservableList<Player> currentTaxi;
    FantasyTeam currentTeam;

    VBox heading;
    VBox playerData;
    HBox draftName;
    HBox edit;

    Label draftNameLabel;
    Label selectFantasyTeamLabel;
    Label lineupLabel;
    Label taxiSquadLabel;
    TextField draftNameField;

    ComboBox<FantasyTeam> teamSelectBox;
    ObservableList<FantasyTeam> comboBoxData;

    Button addTeamButton;
    Button removeTeamButton;
    Button editTeamButton;

    TableColumn fixedPosition;
    TableColumn notes;
    TableColumn fName;
    TableColumn lName;
    TableColumn value;
    TableColumn team;
    TableColumn positions;
    TableColumn<Player, Integer> r_w;
    TableColumn<Player, Integer> hr_sv;
    TableColumn<Player, Integer> rbi_k;
    TableColumn<Player, Double> sb_era;
    TableColumn<Player, Double> ba_whip;
    TableColumn contract;
    TableColumn salary;

    FantasyTeamsScreen(Stage initPrimaryStage, DraftDataManager dataManager, MessageDialog messageDialog, ImageLoader imageLoader, DraftEditController draftController) {
        this.primaryStage = initPrimaryStage;
        this.dataManager = dataManager;
        this.draft = this.dataManager.getDraft();
        this.messageDialog = messageDialog;
        this.imageLoader = imageLoader;
        this.draftController = draftController;
        componentMaker = new INITComponents();
        playerController = new PlayerController(initPrimaryStage, dataManager, messageDialog, imageLoader, draftController);
    }

    public void initComponents() {
        comboBoxData = this.draft.getFantasyTeams();
        teamSelectBox = new ComboBox<FantasyTeam>();
        teamSelectBox.setItems(comboBoxData);
        draftNameField = new TextField();
        fantasyTeamLabel = componentMaker.initChildLabel("Fantasy Teams", CLASS_HEADING_LABEL);
        draftNameLabel = componentMaker.initChildLabel("Draft Name", CLASS_TEXT_LABEL);
        selectFantasyTeamLabel = componentMaker.initChildLabel("Select Fantasy Team", CLASS_TEXT_LABEL);
        lineupLabel = componentMaker.initChildLabel("Starting Lineup", CLASS_SUBHEADING_LABEL);
        taxiSquadLabel = componentMaker.initChildLabel("Taxi Squad", CLASS_SUBHEADING_LABEL);
        addTeamButton = componentMaker.initChildButton(fantasyTeamsPane, WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_TEAM_TOOLTIP, false);
        removeTeamButton = componentMaker.initChildButton(fantasyTeamsPane, WDK_PropertyType.REMOVE_ICON, WDK_PropertyType.REMOVE_TEAM_TOOLTIP, true);
        editTeamButton = componentMaker.initChildButton(fantasyTeamsPane, WDK_PropertyType.EDIT_ICON, WDK_PropertyType.EDIT_TEAM_TOOLTIP, true);
        draftNameLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        selectFantasyTeamLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
    }

    void initFantasyTeamsScreen() {
        fantasyTeamsPane = new BorderPane();
        lineupPane = new BorderPane();
        lineupPane.getStyleClass().add(CLASS_BORDERED_PANE);
        taxiSquadPane = new BorderPane();
        taxiSquadPane.getStyleClass().add(CLASS_BORDERED_PANE);

        fantasyTeamsPane.getStyleClass().add(CLASS_BORDERED_PANE);
        initComponents();
        initLineupTable();
        initTaxiSquadTable();
        initEventHandlers();

        heading = new VBox();
        edit = new HBox();
        draftName = new HBox();

        draftName.getChildren().add(draftNameLabel);
        draftName.getChildren().add(draftNameField);

        edit.getChildren().add(addTeamButton);
        edit.getChildren().add(removeTeamButton);
        edit.getChildren().add(editTeamButton);
        edit.getChildren().add(selectFantasyTeamLabel);
        edit.getChildren().add(teamSelectBox);

        heading.getChildren().add(fantasyTeamLabel);
        heading.getChildren().add(draftName);
        heading.getChildren().add(edit);

        heading.getStyleClass().add(CLASS_HEADING_BOX);

        lineupPane.setTop(lineupLabel);
        lineupPane.setBottom(startingLineupTable);

        taxiSquadPane.setTop(taxiSquadLabel);
        taxiSquadPane.setBottom(taxiSquadTable);

        fantasyTeamsPane.setTop(heading);
        fantasyTeamsPane.setCenter(lineupPane);
        fantasyTeamsPane.setBottom(taxiSquadPane);
    }

    public void initLineupTable() {
        startingLineupTable = new TableView();
        startingLineupTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        // CREATE CUSTOM COMPARATOR TO SORT BY 
        fixedPosition = new TableColumn("Position");
        fName = new TableColumn("First");
        lName = new TableColumn("Last");
        team = new TableColumn("Pro Team");
        positions = new TableColumn("Positions");
        salary = new TableColumn("Salary");
        contract = new TableColumn("Contract");
        r_w = new TableColumn<Player, Integer>("R/W");
        hr_sv = new TableColumn<Player, Integer>("HR/SV");
        rbi_k = new TableColumn<Player, Integer>("RBI/K");
        sb_era = new TableColumn<Player, Double>("SB/ERA");
        ba_whip = new TableColumn<Player, Double>("BA/WHIP");
        value = new TableColumn("Estimated Value");

        fName.setCellValueFactory(new PropertyValueFactory<String, String>("fName"));
        lName.setCellValueFactory(new PropertyValueFactory<String, String>("lName"));
        team.setCellValueFactory(new PropertyValueFactory<String, String>("proTeam"));
        fixedPosition.setCellValueFactory(new PropertyValueFactory<String, String>("fixedPosition"));
        positions.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));
        salary.setCellValueFactory(new PropertyValueFactory<String, Integer>("salary"));
        contract.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        value.setCellValueFactory(new PropertyValueFactory<Player, Integer>("estimatedValue"));

        r_w.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                if (p.getValue() instanceof Pitcher) {
                    Integer wins = ((Pitcher) p.getValue()).getWins();
                    if (wins == 0) {
                        return new ReadOnlyObjectWrapper(null);
                    }
                    return new ReadOnlyObjectWrapper<Integer>(wins);
                } else if (p.getValue() instanceof Hitter) {
                    Integer runs = ((Hitter) p.getValue()).getRuns();
                    return new ReadOnlyObjectWrapper<Integer>(runs);
                }
                return null;
            }
        });

        hr_sv.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                if (p.getValue() instanceof Pitcher) {
                    Integer sv = ((Pitcher) p.getValue()).getSaves();
                    return new ReadOnlyObjectWrapper<Integer>(sv);
                } else if (p.getValue() instanceof Hitter) {
                    Integer hr = ((Hitter) p.getValue()).getHomeRuns();
                    return new ReadOnlyObjectWrapper<Integer>(hr);
                }
                return null;
            }
        });

        rbi_k.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {

            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                if (p.getValue() instanceof Pitcher) {
                    Integer k = ((Pitcher) p.getValue()).getStrikeOuts();
                    if (k == 0) {
                        return new ReadOnlyObjectWrapper(null);
                    }
                    return new ReadOnlyObjectWrapper<Integer>(k);
                } else if (p.getValue() instanceof Hitter) {
                    Integer rbi = ((Hitter) p.getValue()).getRbi();
                    return new ReadOnlyObjectWrapper<Integer>(rbi);
                }
                return null;
            }
        });

        sb_era.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {
            public ObservableValue<Double> call(TableColumn.CellDataFeatures<Player, Double> p) {
                if (p.getValue() instanceof Pitcher) {
                    Double era = ((Pitcher) p.getValue()).getEra();

                    return new ReadOnlyObjectWrapper<Double>(era);
                } else if (p.getValue() instanceof Hitter) {
                    double sb = ((Hitter) p.getValue()).getStolenBases();
                    sb = Math.round(sb);
                    return new ReadOnlyObjectWrapper<Double>(sb);
                }
                return null;
            }
        });

        ba_whip.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {
            public ObservableValue<Double> call(TableColumn.CellDataFeatures<Player, Double> p) {
                if (p.getValue() instanceof Pitcher) {
                    Double whip = ((Pitcher) p.getValue()).getWhip();
                    return new ReadOnlyObjectWrapper<Double>(whip);
                } else if (p.getValue() instanceof Hitter) {
                    Double ba = ((Hitter) p.getValue()).getBa();
                    return new ReadOnlyObjectWrapper<Double>(ba);
                }
                return null;
            }
        });
        startingLineupTable.getColumns().add(fixedPosition);
        startingLineupTable.getColumns().add(fName);
        startingLineupTable.getColumns().add(lName);
        startingLineupTable.getColumns().add(team);
        startingLineupTable.getColumns().add(positions);
        startingLineupTable.getColumns().add(r_w);
        startingLineupTable.getColumns().add(hr_sv);
        startingLineupTable.getColumns().add(rbi_k);
        startingLineupTable.getColumns().add(sb_era);
        startingLineupTable.getColumns().add(ba_whip);
        startingLineupTable.getColumns().add(value);
        startingLineupTable.getColumns().add(contract);
        startingLineupTable.getColumns().add(salary);
        startingLineupTable.setItems(null);
        startingLineupTable.setEditable(true);

    }

    public void initTaxiSquadTable() {
        taxiSquadTable = new TableView();
        taxiSquadTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        // CREATE CUSTOM COMPARATOR TO SORT BY 
        fixedPosition = new TableColumn("Position");
        fName = new TableColumn("First");
        lName = new TableColumn("Last");
        team = new TableColumn("Pro Team");
        positions = new TableColumn("Positions");
        salary = new TableColumn("Salary");
        contract = new TableColumn("Contract");
        r_w = new TableColumn<Player, Integer>("R/W");
        hr_sv = new TableColumn<Player, Integer>("HR/SV");
        rbi_k = new TableColumn<Player, Integer>("RBI/K");
        sb_era = new TableColumn<Player, Double>("SB/ERA");
        ba_whip = new TableColumn<Player, Double>("BA/WHIP");
        value = new TableColumn("Estimated Value");

        fName.setCellValueFactory(new PropertyValueFactory<String, String>("fName"));
        lName.setCellValueFactory(new PropertyValueFactory<String, String>("lName"));
        team.setCellValueFactory(new PropertyValueFactory<String, String>("proTeam"));
        fixedPosition.setCellValueFactory(new PropertyValueFactory<String, String>("fixedPosition"));
        positions.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));
        salary.setCellValueFactory(new PropertyValueFactory<String, Integer>("salary"));
        contract.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));

        r_w.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                if (p.getValue() instanceof Pitcher) {
                    Integer wins = ((Pitcher) p.getValue()).getWins();
                    if (wins == 0) {
                        return new ReadOnlyObjectWrapper(null);
                    }
                    return new ReadOnlyObjectWrapper<Integer>(wins);
                } else if (p.getValue() instanceof Hitter) {
                    Integer runs = ((Hitter) p.getValue()).getRuns();
                    return new ReadOnlyObjectWrapper<Integer>(runs);
                }
                return null;
            }
        });

        hr_sv.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                if (p.getValue() instanceof Pitcher) {
                    Integer sv = ((Pitcher) p.getValue()).getSaves();
                    return new ReadOnlyObjectWrapper<Integer>(sv);
                } else if (p.getValue() instanceof Hitter) {
                    Integer hr = ((Hitter) p.getValue()).getHomeRuns();
                    return new ReadOnlyObjectWrapper<Integer>(hr);
                }
                return null;
            }
        });

        rbi_k.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {

            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                if (p.getValue() instanceof Pitcher) {
                    Integer k = ((Pitcher) p.getValue()).getStrikeOuts();
                    if (k == 0) {
                        return new ReadOnlyObjectWrapper(null);
                    }
                    return new ReadOnlyObjectWrapper<Integer>(k);
                } else if (p.getValue() instanceof Hitter) {
                    Integer rbi = ((Hitter) p.getValue()).getRbi();
                    return new ReadOnlyObjectWrapper<Integer>(rbi);
                }
                return null;
            }
        });

        sb_era.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {
            public ObservableValue<Double> call(TableColumn.CellDataFeatures<Player, Double> p) {
                if (p.getValue() instanceof Pitcher) {
                    Double era = ((Pitcher) p.getValue()).getEra();

                    return new ReadOnlyObjectWrapper<Double>(era);
                } else if (p.getValue() instanceof Hitter) {
                    double sb = ((Hitter) p.getValue()).getStolenBases();
                    sb = Math.round(sb);
                    return new ReadOnlyObjectWrapper<Double>(sb);
                }
                return null;
            }
        });

        ba_whip.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {
            public ObservableValue<Double> call(TableColumn.CellDataFeatures<Player, Double> p) {
                if (p.getValue() instanceof Pitcher) {
                    Double whip = ((Pitcher) p.getValue()).getWhip();
                    return new ReadOnlyObjectWrapper<Double>(whip);
                } else if (p.getValue() instanceof Hitter) {
                    Double ba = ((Hitter) p.getValue()).getBa();
                    return new ReadOnlyObjectWrapper<Double>(ba);
                }
                return null;
            }
        });
        taxiSquadTable.getColumns().add(fixedPosition);
        taxiSquadTable.getColumns().add(fName);
        taxiSquadTable.getColumns().add(lName);
        taxiSquadTable.getColumns().add(team);
        taxiSquadTable.getColumns().add(positions);
        taxiSquadTable.getColumns().add(r_w);
        taxiSquadTable.getColumns().add(hr_sv);
        taxiSquadTable.getColumns().add(rbi_k);
        taxiSquadTable.getColumns().add(sb_era);
        taxiSquadTable.getColumns().add(ba_whip);
        taxiSquadTable.getColumns().add(value);
        taxiSquadTable.getColumns().add(contract);
        taxiSquadTable.getColumns().add(salary);
        taxiSquadTable.setItems(null);
        taxiSquadTable.setEditable(true);
    }

    public BorderPane getFantasyTeamsScreen() {
        return fantasyTeamsPane;
    }

    public void initDefaultTables() {

    }

    private void setCurrentTeam() {
        System.out.println("SET CURRENT TEAM");
        if (currentTeam == null) {
            startingLineupTable.setItems(null);
            taxiSquadTable.setItems(null);
        } else {
            startingLineupTable.setItems(currentTeam.getRoster());
            taxiSquadTable.setItems(currentTeam.getTaxiSquad());
        }
    }

    private void initEventHandlers() {
        if (currentTeam == null) {
            removeTeamButton.setDisable(true);
            editTeamButton.setDisable(true);
        }
        fantasyController = new FantasyTeamController(primaryStage, dataManager, messageDialog, draftController);

        addTeamButton.setOnAction(e -> {
            fantasyController.handleAddTeamRequest();
        });
        removeTeamButton.setOnAction(e -> {
            if (currentTeam != null) {
                fantasyController.handleRemoveTeamRequest(currentTeam);
                comboBoxData.add(dummyTeam);
                comboBoxData.remove(dummyTeam);
                if (comboBoxData.isEmpty()) {
                    editTeamButton.setDisable(true);
                }
            }
        });
        editTeamButton.setOnAction(e -> {
            fantasyController.handleEditTeamRequest(currentTeam);
            comboBoxData.add(dummyTeam);
            comboBoxData.remove(dummyTeam);
        });

        teamSelectBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            currentTeam = newValue;
            setCurrentTeam();
            editTeamButton.setDisable(false);
            removeTeamButton.setDisable(false);
        });
        startingLineupTable.setOnMouseClicked(e -> {
            currentPlayer = startingLineupTable.getSelectionModel().getSelectedItem();
            if (e.getClickCount() == 2) {
                if (currentPlayer == null) {
                    System.out.println("player is null");
                } else {
                    System.out.println("name is null");
                }
                System.out.println(currentPlayer.getLName());
                playerController.handleEditPlayerRequest(currentPlayer);
            }
        });
    }

    public DraftDataManager getDataManager() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getDraftName() {
        return draftNameField.getText();
    }
}
