package wdk.gui;

import javafx.scene.control.Label;

public class LabelMaker {

    public Label initLabel(String labelText, String styleClass) {
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }

    public Label initChildLabel(String labelText, String styleClass) {
        Label label = initLabel(labelText, styleClass);
        return label;
    }

}
