package wdk.gui;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wdk.data.Draft;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.data.FantasyTeam;
import wdk.error.CantAffordException;
import wdk.error.TooManyPitchersException;
import wdk.error.WrongHitterPositionException;
import static wdk.gui.FantasyTeamsScreen.PRIMARY_STYLE_SHEET;
import wdk.file.ImageLoader;

public class PlayerDialog extends Stage {

    //ImageLoader imageLoader;
    Player currentPlayer;
    Player newPlayer;
    Draft draft;
    ObservableList<String> availablePositions;
    FantasyTeam fantasyTeam;

    MessageDialog messageDialog;

    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Stage primaryStage;

    // NEW PLAYER 
    TextField fNameTextField;
    Label fNameLabel;
    TextField lNameTextField;
    Label lNameLabel;
    Label proTeamLabel;
    ComboBox proTeamBox;
    ObservableList<String> proTeamData;
    // POSITION CHECK BOXES
    HBox positionsHBox;
    CheckBox catcher;
    CheckBox firBase;
    CheckBox thrBase;
    CheckBox secBase;
    CheckBox shortStop;
    CheckBox outField;
    CheckBox pitcher;

    // EDIT EXISTING PLAYER
    ImageView playerImg;
    ImageView playerFlag;
    Label playerName;
    Label playerPositions;
    Label fantasyTeamLabel;
    ComboBox<FantasyTeam> fantasyTeamBox;
    Label positionLabel;
    ComboBox positionsBox;
    Label contractLabel;
    ComboBox contractBox;
    Label salaryLabel;
    TextField salaryField;
    ObservableList<String> contractData;
    Button freeAgentButton;
    ObservableList<String> fullData;

    Button completeButton;
    Button cancelButton;

    String selection;

    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
//    public static final String NAME_PROMPT = "Name: ";
//    public static final String OWNER_PROMPT = "Owner: ";
    public static final String CLASS_DIALOG_HEADING = "dialog_heading";
    public static final String TEAM_HEADING = "Player Details";
    public static final String CLASS_HEADING_LABEL = "heading_label";
    public static final String CLASS_DIALOG_SUBHEADING_LABEL = "dialog_subheading_label";
    public static final String CLASS_PROMPT_LABEL = "prompt_label";
    public static final String ADD_PLAYER_TITLE = "Add New Player";
    public static final String EDIT_TEAM_TITLE = "Edit Player";

    public PlayerDialog(Stage primaryStage, MessageDialog messageDialog, Draft draft) {
        newPlayer = new Player();
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        this.primaryStage = primaryStage;
        gridPane = new GridPane();
        this.messageDialog = messageDialog;
        this.draft = draft;
        //imageLoader
        initAddComponents();
        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");

        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        headingLabel = new Label(TEAM_HEADING);
        headingLabel.getStyleClass().add(CLASS_DIALOG_HEADING);

        gridPane.add(headingLabel, 0, 0, 3, 1);

        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);

    }

    public void initEditComponents() {
        freeAgentButton = new Button("Undraft Player");
        playerImg = new ImageView();
        playerFlag = new ImageView();
        playerName = new Label();
        playerName.getStyleClass().add(CLASS_DIALOG_SUBHEADING_LABEL);
        positionLabel = new Label("Position:");
        fantasyTeamLabel = new Label("Fantasy Team:");
        playerPositions = new Label();
        playerPositions.getStyleClass().add(CLASS_DIALOG_SUBHEADING_LABEL);
        contractLabel = new Label("Contract:");
        salaryLabel = new Label("Salary:");

        contractData = FXCollections.observableArrayList("S2", "S1");//, "X");
        fullData = FXCollections.observableArrayList("X");

        fantasyTeamBox = new ComboBox();
        positionsBox = new ComboBox();
        contractBox = new ComboBox();
        salaryField = new TextField();
        contractBox.setItems(contractData);

        availablePositions = FXCollections.observableArrayList();

        fantasyTeamBox.setItems(draft.getFantasyTeams());

    }

    public void initAddComponents() {
        positionsHBox = new HBox();
        fNameTextField = new TextField();
        fNameLabel = new Label("First Name:");
        lNameTextField = new TextField();
        lNameLabel = new Label("Last Name: ");
        proTeamBox = new ComboBox();
        proTeamLabel = new Label("Pro Team:");
        // POSITION CHECK BOXES
        catcher = new CheckBox("C");
        firBase = new CheckBox("1B");
        thrBase = new CheckBox("3B");
        secBase = new CheckBox("2B");
        shortStop = new CheckBox("SS");
        outField = new CheckBox("OF");
        pitcher = new CheckBox("P");

        positionsHBox.getChildren().add(catcher);//, 1, 1);
        positionsHBox.getChildren().add(firBase);//, 1, 4);//, 1, 1);
        positionsHBox.getChildren().add(thrBase);//, 2, 4);//, 1, 1);
        positionsHBox.getChildren().add(secBase);//, 3, 4);//, 1, 1);
        positionsHBox.getChildren().add(shortStop);//, 4, 4);//, 1, 1);
        positionsHBox.getChildren().add(outField);//, 5, 4);//, 1, 1);
        positionsHBox.getChildren().add(pitcher);//, 6, 4);//, 1, 1);

        proTeamData = FXCollections.observableArrayList("ATL", "AZ", "CHC", "CIN",
                "COL", "LAD", "MIA", "MIL", "NYM", "PHI", "PIT",
                "SD", "SF", "STL", "WAS");
        proTeamBox.setItems(proTeamData);

        //headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        fNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        lNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        proTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        // EDIT EXISTING PLAYER

    }

    public void showAddPlayerDialog() {
        setTitle(ADD_PLAYER_TITLE);

        //newPlayer = new Player();
        fNameTextField.setText(newPlayer.getFName());
        lNameTextField.setText(newPlayer.getLName());

        gridPane.add(fNameLabel, 0, 1, 1, 1);
        gridPane.add(fNameTextField, 1, 1, 1, 1);
        gridPane.add(lNameLabel, 0, 2, 1, 1);
        gridPane.add(lNameTextField, 1, 2, 1, 1);
        gridPane.add(proTeamLabel, 0, 3, 1, 1);
        gridPane.add(proTeamBox, 1, 3, 1, 1);

        gridPane.add(positionsHBox, 0, 4);

        gridPane.add(completeButton, 0, 5, 1, 1);
        gridPane.add(cancelButton, 1, 5, 1, 1);

        initAddEventHandlers();

        this.showAndWait();

    }

    public boolean wasCompleteSelected() {
        if (selection == null) {
            return false;
        }
        return selection.equals(COMPLETE);
    }

    public Player getPlayer() {
        return newPlayer;
    }

    public void initEditHandlers() {
        completeButton.setOnAction(e -> {
            boolean errorFree = true;
            if (fantasyTeamBox.getSelectionModel().getSelectedItem() == null
                    || salaryField.getText() == null || positionsBox.getSelectionModel().getSelectedItem() == null
                    || contractBox.getSelectionModel().getSelectedItem() == null) {
                errorFree = false;
                messageDialog.show("Please fill out all fields");
            } else if (!fantasyTeamBox.getSelectionModel().getSelectedItem().affordSalary(Integer.valueOf(salaryField.getText()))) {
                errorFree = false;
                messageDialog.show("Can afford "
                        + fantasyTeamBox.getSelectionModel().getSelectedItem().getHighestBid()
                        + " at most.");

            } else {
                try {
                    String contract = contractBox.getSelectionModel().getSelectedItem().toString();
                    errorFree = true;
                    String pos = currentPlayer.getFixedPosition();
                    currentPlayer.setFixedPosition(positionsBox.getSelectionModel().getSelectedItem().toString());
                    if (contract.equals("X")) {
                        messageDialog.show("Taxi Squad Players\nautomatically given $1 Salary");
                        currentPlayer.setSalary(1);
                    } else {
                        currentPlayer.setSalary(Integer.valueOf(salaryField.getText()));
                    }
                    FantasyTeam fanTeam = fantasyTeamBox.getSelectionModel().getSelectedItem();

                    if (currentPlayer.isFreeAgent()) {  // Free agent -> drafted
                        try {
                            currentPlayer.setContract(contract);
                            fanTeam.addPlayer(currentPlayer);
                            if (currentPlayer.getContract().equals("S2")) {// || currentPlayer.getContract().equals("x")) {
                                draft.getDraftOrder().add(currentPlayer);
                            }
                            currentPlayer.setFanTeamName(fanTeam.getTeamName());
                            draft.getPlayerPool().remove(currentPlayer);
                            currentPlayer.setFreeAgent(false);

                            System.out.println("TOO MANY PITCHERS");
                        } catch (CantAffordException ex) {
                            Logger.getLogger(PlayerDialog.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else { // Drafted -> Drafted 
                        try {
                            if (contract.equals("S2")) { // CHOSEN CONTRACT WILL BE ON DRAFT-ORDER 
                                // PLAYER WAS NOT PREVIOUSLY ON DRAFT-ORDER 
                                if (currentPlayer.getContract().equals("S1") || currentPlayer.getContract().equals("X")) {
                                    draft.getDraftOrder().add(currentPlayer);
                                    currentPlayer.setContract(contract);
                                    fanTeam.refresh();
                                }
                            } else {
                                if (currentPlayer.getContract().equals("S2")) { // PLAYER WAS ON DRAFT-ORDER 
                                    draft.getDraftOrder().remove(currentPlayer);
                                    currentPlayer.setContract(contract);
                                    fanTeam.refresh();
                                }
                            }
                            if (fanTeam.getRoster().contains(currentPlayer)) { //Switching position on same team 
                                fanTeam.getRoster().remove(currentPlayer);
                                fanTeam.getPositionsNeeded().add(pos);
                                currentPlayer.setContract(contract);
                                fanTeam.addPlayer(currentPlayer);

                            } else { // Switching to new team
                                for (FantasyTeam ft : draft.getFantasyTeams()) {
                                    if (ft.getRoster().contains(currentPlayer)) {
                                        ft.getPositionsNeeded().add(pos);
                                        ft.getRoster().remove(currentPlayer);
                                        break;
                                    }
                                }
                                currentPlayer.setContract(contract);
                                fanTeam.addPlayer(currentPlayer);
//                                currentPlayer.getFantasyTeam().removePlayer(currentPlayer);
//                                currentPlayer.getFantasyTeam().getPositionsNeeded().add(pos);
                                currentPlayer.setFanTeamName(fanTeam.getTeamName());

                            }

                        } catch (CantAffordException ex) {
                            Logger.getLogger(PlayerDialog.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                } catch (NumberFormatException ex) {
                    errorFree = false;
                    messageDialog.show("Please enter a numerical salary");
                }

            }
            if (errorFree) {
                this.hide();
            }

        });

        selection = completeButton.getText();

        cancelButton.setOnAction(e -> {
            selection = cancelButton.getText();
            this.hide();
        });

        freeAgentButton.setOnAction(e -> {
            //draft.getDraftOrder().add(currentPlayer.getName() + " moved from " + currentPlayer.getFantasyTeam().getTeamName() + " to player pool");
            draft.getDraftOrder().remove(currentPlayer);
            String pos = currentPlayer.getFixedPosition();
            draft.getPlayerPool().add(currentPlayer);
            for (FantasyTeam ft : draft.getFantasyTeams()) {
                if (ft.getRoster().contains(currentPlayer)) {
                    ft.getPositionsNeeded().add(pos);
                    ft.getRoster().remove(currentPlayer);
                    break;
                }
            }
            currentPlayer.setContract(null);
            currentPlayer.setSalary(-1);
            currentPlayer.setFantasyTeam(null);
            currentPlayer.setFreeAgent(true);
            this.hide();
        });
    }

    public void setAllPositions(Player player) {

            ObservableList<String> qualifiedPositions = player.getPositionsList();

            for (int i = 0; i < qualifiedPositions.size(); i++) {

                availablePositions.add(qualifiedPositions.get(i));

            }
            Set<String> hs = new HashSet<>();
            hs.addAll(availablePositions);
            availablePositions.clear();
            availablePositions.addAll(hs);
        
    }

    public void setAvailPositions(FantasyTeam team, Player player) {
        if (team != null) {
            ObservableList<String> neededPositions = team.getPositionsNeeded();
            ObservableList<String> qualifiedPositions = player.getPositionsList();

            for (int i = 0; i < qualifiedPositions.size(); i++) {
                for (int j = 0; j < neededPositions.size(); j++) {
                    if (qualifiedPositions.get(i).equals(neededPositions.get(j))) {
                        availablePositions.add(qualifiedPositions.get(i));
                    }
                }
            }
            Set<String> hs = new HashSet<>();
            hs.addAll(availablePositions);
            availablePositions.clear();
            availablePositions.addAll(hs);
        }
    }

    public void showEditPlayerDialog(Player player, ImageLoader imageLoader) {
        initEditComponents();
        setTitle("Edit Player Dialog");
        currentPlayer = player;
        if (player.isFreeAgent()) {
            freeAgentButton.setDisable(true);
        } else {
            freeAgentButton.setDisable(false);
        }

        if (imageLoader.getPlayerImages().get(player.getIMGName()) == null) {
            playerImg.setImage(imageLoader.getPlayerImages().get("unknown"));
        } else {
            playerImg.setImage(imageLoader.getPlayerImages().get(player.getIMGName()));
        }
        if (imageLoader.getFlags().get(player.getNOB()) == null) {
            playerFlag.setImage(imageLoader.getFlags().get("unknown"));
        } else {
            playerFlag.setImage(imageLoader.getFlags().get(player.getNOB()));
        }

        playerName.setText(player.getName());
        playerPositions.setText(player.getPositions());

        fantasyTeamBox.setOnAction(e -> {
            FantasyTeam tempTeam = fantasyTeamBox.getSelectionModel().getSelectedItem();
//            System.out.println(tempTeam == null);
//            if (tempTeam.isFull()) {
//                setAllPositions(currentPlayer);
//                if (tempTeam.getTaxiSquad().size() < 8) {
//                    contractData.add("X");
//                }
//            }
            setAvailPositions(tempTeam, player);
            if (tempTeam != null && tempTeam.isFull()) {
                setAllPositions(currentPlayer);
                contractBox.setItems(fullData);
            }
            positionsBox.setItems(availablePositions);
        });

        gridPane.add(playerImg, 0, 1, 1, 3);
        gridPane.add(playerFlag, 1, 1, 2, 1);
        gridPane.add(playerName, 1, 2, 3, 1);
        gridPane.add(playerPositions, 1, 3, 1, 1);
        gridPane.add(fantasyTeamLabel, 0, 4, 1, 1);
        gridPane.add(fantasyTeamBox, 1, 4, 3, 1);
        gridPane.add(positionLabel, 0, 5, 1, 1);
        gridPane.add(positionsBox, 1, 5, 3, 1);
        gridPane.add(contractLabel, 0, 6, 1, 1);
        gridPane.add(contractBox, 1, 6, 3, 1);
        gridPane.add(salaryLabel, 0, 7, 1, 1);
        gridPane.add(salaryField, 1, 7, 3, 1);
        gridPane.add(freeAgentButton, 0, 8, 1, 1);
        gridPane.add(completeButton, 0, 9, 1, 1);
        gridPane.add(cancelButton, 1, 9, 1, 1);

        initEditHandlers();
        this.showAndWait();
    }

    public boolean isPitcher() {
        return pitcher.isSelected();
    }

    public boolean isHitter() {
        return (catcher.isSelected() || firBase.isSelected() || thrBase.isSelected()
                || secBase.isSelected() || shortStop.isSelected() || outField.isSelected());
    }

    public ArrayList<String> getHitterPositions() {
        ArrayList<String> list = new ArrayList<String>();
        if (catcher.isSelected()) {
            list.add(catcher.getText());
        }
        if (firBase.isSelected()) {
            list.add(firBase.getText());
        }
        if (thrBase.isSelected()) {
            list.add(thrBase.getText());

        }
        if (secBase.isSelected()) {
            list.add(secBase.getText());

        }
        if (shortStop.isSelected()) {
            list.add(shortStop.getText());

        }
        if (outField.isSelected()) {
            list.add(outField.getText());
        }
        return list;
    }

    public void setTextHandlers() {
        newPlayer.setfName(fNameTextField.getText());
        newPlayer.setlName(lNameTextField.getText());
        newPlayer.setProTeam(proTeamBox.getSelectionModel().getSelectedItem().toString());
    }

    public void initAddEventHandlers() {

        completeButton.setOnAction(e -> {
            boolean errorFree = true;
            if (isPitcher() && isHitter()) {
                messageDialog.show("Player cannot be a pitcher in addition to other positions.");
                errorFree = false;
            } else if ((!isPitcher() && !isHitter()) || fNameTextField.getText().length() < 1
                    || lNameTextField.getText().length() < 1
                    || proTeamBox.getSelectionModel().getSelectedItem() == null) {
                messageDialog.show("Please complete all fields");
                errorFree = false;
            }
            if (pitcher.isSelected()) {
                newPlayer = new Pitcher();
                newPlayer.setPositions("P");
                setTextHandlers();
            } else if (isHitter()) {
                newPlayer = new Hitter();
                newPlayer.setFreeAgent(true);
                newPlayer.setPositions(join(getHitterPositions()));
                newPlayer.setQp(join(getHitterPositions()));
                setTextHandlers();
            }

            if (errorFree) {
                this.hide();
            }
        });

        selection = completeButton.getText();

        cancelButton.setOnAction(e -> {
            selection = cancelButton.getText();

            this.hide();
        });

    }

    public String join(ArrayList<String> list) {
        String pos = "";
        for (int i = 0; i < list.size(); i++) {
            pos += list.get(i);
            if (i != list.size() - 1) {
                pos += "_";
            }
        }
        return pos;
    }

    public void unselectCheckBoxes() {
        catcher.setSelected(false);
        firBase.setSelected(false);
        thrBase.setSelected(false);
        secBase.setSelected(false);
        shortStop.setSelected(false);
        outField.setSelected(false);
        pitcher.setSelected(false);
    }

    public void playerErrorHandler() {

    }

}
