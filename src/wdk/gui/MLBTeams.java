package wdk.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static wdk.WDK_INITConstants.PATH_CSS;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.Player;

public class MLBTeams {

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "csb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_TOGGLE_GROUP = "toggle_group";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    Stage primaryStage;
    Draft draft;
    DraftDataManager dataManager;
    BorderPane mlbTeamScreen;
    Label mlbTeamLabel;
    Label teamSelectLabel;
    FilteredList<Player> filteredPlayers;
    SortedList<Player> sortedPlayers;
    INITComponents componentMaker;
    TableView proTeamTable;
    ComboBox proTeamBox;
    ObservableList<String> proTeamData;
    
    TableColumn<Player, String> firstName;
    TableColumn<Player, String> lastName;
    TableColumn<Player, String> positions;

    VBox dataVBox;
    HBox proTeamSelect;
    

    MLBTeams(Stage initPrimaryStage, DraftDataManager dataManager) {
        this.primaryStage = initPrimaryStage;
        this.dataManager = dataManager;
        filteredPlayers = new FilteredList<>(dataManager.getDraft().getAllPlayers(), p -> false);
        sortedPlayers = new SortedList<>(filteredPlayers);
        componentMaker = new INITComponents();
    }

    public void initComponents() {
        mlbTeamScreen = new BorderPane();
        dataVBox = new VBox();
        proTeamSelect = new HBox();
        proTeamBox = new ComboBox();
        mlbTeamLabel = componentMaker.initChildLabel("MLB Teams", CLASS_HEADING_LABEL);
        teamSelectLabel = componentMaker.initChildLabel("Select Pro Team", CLASS_SUBHEADING_LABEL);
        proTeamTable = new TableView();
        
        proTeamData = FXCollections.observableArrayList("ATL", "AZ", "CHC", "CIN",
                "COL", "LAD", "MIA", "MIL", "NYM", "PHI", "PIT",
                "SD", "SF", "STL", "WAS");
        proTeamBox.setItems(proTeamData);
    }

    public void initMLBTeamsScreen() {
        initComponents();
        initEventHandlers();
        initMLBTable();
        mlbTeamScreen.getStyleClass().add(CLASS_BORDERED_PANE);
        proTeamSelect.getChildren().add(teamSelectLabel);
        proTeamSelect.getChildren().add(proTeamBox);
        dataVBox.getChildren().add(mlbTeamLabel);
        dataVBox.getChildren().add(proTeamSelect);
        dataVBox.getChildren().add(proTeamTable);
        mlbTeamScreen.setCenter(dataVBox);
    }

    public void initMLBTable() {
        sortedPlayers.comparatorProperty().bind(proTeamTable.comparatorProperty());
        proTeamTable.setMaxWidth(400);
        proTeamTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                
        firstName = new TableColumn("First Name");
        lastName = new TableColumn("Last Name");
        positions = new TableColumn("Positions");
        
        firstName.setCellValueFactory(new PropertyValueFactory<Player, String>("fName"));
        lastName.setCellValueFactory(new PropertyValueFactory<Player, String>("lName"));
        positions.setCellValueFactory(new PropertyValueFactory<Player, String>("positions"));
        
        proTeamTable.getColumns().add(firstName);
        proTeamTable.getColumns().add(lastName);
        proTeamTable.getColumns().add(positions);
        
        proTeamTable.setItems(sortedPlayers);
        proTeamTable.getSortOrder().add(lastName);
       
    }

    public void initEventHandlers() {
        proTeamBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            filteredPlayers.setPredicate(player -> {
                if (player.getProTeam().equals(newValue)){
                    return true;
                }
                else
                    return false;
            });
        });
    }
    
    public BorderPane getMLBTeamsScreen() {
        return mlbTeamScreen;
    }
}
