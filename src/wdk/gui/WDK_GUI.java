package wdk.gui;

//import csb.CSB_PropertyType;
//import csb.controller.CourseEditController;
//import csb.data.Course;
//import csb.data.CourseDataManager;
//import csb.data.CourseDataView;
//import csb.data.CoursePage;
//import csb.controller.FileController;
//import csb.controller.ScheduleEditController;
//import csb.controller.LectureEditController;
//import csb.controller.AssignmentEditController;
//import csb.controller.ProgressController;
//import csb.data.Instructor;
//import csb.data.ScheduleItem;
//import csb.data.Lecture;
//import csb.data.Assignment;
//import csb.data.Semester;
//import csb.data.Subject;
//import csb.file.CourseFileManager;
//import csb.file.CourseSiteExporter;
import java.io.IOException;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.NumberStringConverter;
import javax.swing.ButtonGroup;
//import sun.font.TextLabel;
import wdk.data.DraftDataView;
import wdk.gui.PlayersScreen;
import wdk.WDK_INITConstants;
import static wdk.WDK_INITConstants.*;
import wdk.WDK_PropertyType;
import wdk.controller.DraftEditController;
import wdk.controller.FileController;
import wdk.controller.TeamEditController;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.file.DraftExporter;
import wdk.file.DraftFileManager;
import wdk.file.ImageLoader;
import wdk.file.JsonDraftFileManager;

public class WDK_GUI implements DraftDataView {

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_DRAFT_PANE = "draft_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    PlayersScreen playersScreen;
    FantasyTeamsScreen fantasyTeamsScreen;
    MLBTeams mlbTeamsScreen;
    FantasyStandings fantasyStandingsScreen;
    DraftSummary draftSummaryScreen;

    INITComponents componentMaker;

    ObservableList<Player> sortedPlayers;

    DraftDataManager dataManager;

    DraftFileManager draftFileManager;

    FileController fileController;

    JsonDraftFileManager jsonManager;

    DraftExporter draftExporter;

    DraftEditController draftController;

    TeamEditController teamController;

    Stage primaryStage;

    Scene primaryScene;

    BorderPane wdkPane;

    FlowPane fileToolbarPane;
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button exportDraftButton;
    Button exitButton;

    FlowPane screenToolbarPane;
    Button playerScreenButton;
    Button fantasyTeamButton;
    Button fantasyStandingsButton;
    Button draftScreenButton;
    Button MLBTeamsButton;

    TableView<Player> playerTable;

    BorderPane workspacePane;
    boolean workspaceActivated;

    ScrollPane workspaceScrollPane;

    VBox topWorkspacePane;
    Label draftHeadingLabel;
    SplitPane topWorkSpaceSplitPane;

    BorderPane playerScreenPane;
    Button addPlayerButton;
    Button removePlayerButton;

    Label playerScreenHeading = new Label("Available Players");

    BorderPane fantasyTeamsPane;
    BorderPane fantasyStandingsPane;
    BorderPane draftSummaryPane;
    BorderPane MLBTeamsPane;

    MessageDialog messageDialog;

    YesNoCancelDialog yesNoCancelDialog;
    ImageLoader imageLoader;

    public WDK_GUI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    }

    public DraftDataManager getDataManager() {
        return dataManager;
    }

    public FileController getFileController() {
        return fileController;
    }

    public DraftFileManager getCourseFileManager() {
        return draftFileManager;
    }

    public DraftExporter getSiteExporter() {
        return draftExporter;
    }

    public Stage getWindow() {
        return primaryStage;
    }

    public MessageDialog getMessageDialog() {
        return messageDialog;
    }

    public YesNoCancelDialog getYesNoCancelDialog() {
        return yesNoCancelDialog;
    }

    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }

    public void setCourseFileManager(DraftFileManager initCourseFileManager) {
        draftFileManager = initCourseFileManager;
    }

    public void setDraftExporter(DraftExporter initSiteExporter) {
        draftExporter = initSiteExporter;
    }

    public void initGui() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void initGUI(String windowTitle, ArrayList<Player> players) throws IOException {
        componentMaker = new INITComponents();
        initDialogs();
        initFileToolbar();
        initScreenToolbar();
        initWorkspace(players);
        initEventHandlers();
        initWindow(windowTitle);
        jsonManager = new JsonDraftFileManager();
        draftController = new DraftEditController(this.dataManager, this.messageDialog);
    }

    public void activateWorkspace() {
        initScreens();
        wdkPane.setCenter(workspaceScrollPane);
        workspaceScrollPane.setContent(fantasyTeamsScreen.getFantasyTeamsScreen());
        workspaceActivated = true;

    }

    @Override
    public void reloadDraft(Draft draftToReload) {
        if (!workspaceActivated) {
            activateWorkspace();
        }
        draftController.rankTeams();
        draftController.rankPlayers();

        //draftController.enable(false);
        // SET ALL DEFAULT VALUE AFTER DISABLING USER INTERACTION ABOVE
        // THEN REACTIVATE USER INTERACTION BELOW
        //draftController.enable(true);
    }

    public void updateToolbarControls(boolean saved) {
        playerScreenButton.setDisable(false);
        fantasyTeamButton.setDisable(false);
        fantasyStandingsButton.setDisable(false);
        draftScreenButton.setDisable(false);
        MLBTeamsButton.setDisable(false);
    }

    public void updateDraftInfo(Draft draft) {
        //  UPDATE THIS SHIT
        //  WHEN THE USER SAYS SO
    }

    private void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
    }

    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newDraftButton = componentMaker.initChildButton(fileToolbarPane, WDK_PropertyType.NEW_DRAFT_ICON, WDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        loadDraftButton = componentMaker.initChildButton(fileToolbarPane, WDK_PropertyType.LOAD_DRAFT_ICON, WDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        saveDraftButton = componentMaker.initChildButton(fileToolbarPane, WDK_PropertyType.SAVE_DRAFT_ICON, WDK_PropertyType.SAVE_DRAFT_TOOLTIP, false);
        exportDraftButton = componentMaker.initChildButton(fileToolbarPane, WDK_PropertyType.EXPORT_DRAFT_ICON, WDK_PropertyType.EXPORT_DRAFT_TOOLTIP, true);
        exitButton = componentMaker.initChildButton(fileToolbarPane, WDK_PropertyType.EXIT_ICON, WDK_PropertyType.EXIT_TOOLTIP, false);
    }

    private void initScreenToolbar() {
        screenToolbarPane = new FlowPane();
        fantasyTeamButton = componentMaker.initChildButton(screenToolbarPane, WDK_PropertyType.FANTASY_TEAMS_ICON, WDK_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        playerScreenButton = componentMaker.initChildButton(screenToolbarPane, WDK_PropertyType.PLAYER_SCREEN_ICON, WDK_PropertyType.PLAYER_SCREEN_TOOLTIP, false);
        fantasyStandingsButton = componentMaker.initChildButton(screenToolbarPane, WDK_PropertyType.FANTASY_STANDINGS_ICON, WDK_PropertyType.FANTASY_STANDINGS_TOOLTIP, false);
        draftScreenButton = componentMaker.initChildButton(screenToolbarPane, WDK_PropertyType.DRAFT_SCREEN_ICON, WDK_PropertyType.DRAFT_SCREEN_TOOLTIP, false);
        MLBTeamsButton = componentMaker.initChildButton(screenToolbarPane, WDK_PropertyType.MLB_TEAMS_ICON, WDK_PropertyType.MLB_TEAMS_TOOLTIP, false);
    }

    private void initWorkspace(ArrayList<Player> players) {
        initBasicDraftInfoControls(players);
        // AND NOW PUT IT IN THE WORKSPACE
        workspaceScrollPane = new ScrollPane();

        workspaceScrollPane.setFitToWidth(true);
        workspaceScrollPane.setFitToHeight(true);

        // NOTE THAT WE HAVE NOT PUT THE WORKSPACE INTO THE WINDOW,
        // THAT WILL BE DONE WHEN THE USER EITHER CREATES A NEW
        // COURSE OR LOADS AN EXISTING ONE FOR EDITING
        workspaceActivated = false;
    }

    private void initWindow(String windowTitle) {
        primaryStage.setTitle(windowTitle);
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        wdkPane = new BorderPane();
        wdkPane.setTop(fileToolbarPane);
        wdkPane.setCenter(workspaceScrollPane);
        wdkPane.setBottom(screenToolbarPane);
        primaryScene = new Scene(wdkPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    public void setDraftFileManager(JsonDraftFileManager jsonFileManager) {
        draftFileManager = jsonFileManager;
    }

    private void initBasicDraftInfoControls(ArrayList<Player> players) {
        System.out.println("\n\nNOT DONE YET - basicdraftinfocontrols\n\n");
    }

    public void initScreens() {
        playersScreen = new PlayersScreen(primaryStage, dataManager, messageDialog, imageLoader, draftController);
        playersScreen.initPlayersScreen();

        fantasyTeamsScreen = new FantasyTeamsScreen(primaryStage, dataManager, messageDialog, imageLoader, draftController);
        fantasyTeamsScreen.initFantasyTeamsScreen();

        fantasyStandingsScreen = new FantasyStandings(primaryStage, dataManager);
        fantasyStandingsScreen.initFantsyStandingsTeam();

        draftSummaryScreen = new DraftSummary(primaryStage, dataManager, messageDialog, draftController);
        draftSummaryScreen.initDraftSummaryScreen();

        mlbTeamsScreen = new MLBTeams(primaryStage, dataManager);
        mlbTeamsScreen.initMLBTeamsScreen();

    }

    private void initEventHandlers() {
        fileController = new FileController(messageDialog, yesNoCancelDialog, draftFileManager);
        newDraftButton.setOnAction(e -> {
            fileController.handleNewDraftRequest(this);
        });

        saveDraftButton.setOnAction(e -> {
            String draftName = fantasyTeamsScreen.getDraftName();
            if (draftName.equals("")) {
                messageDialog.show("Please give Draft a name");
            } else {
                if (dataManager.getDraft() == null) {
                    System.out.println("THIS DRAFT IS NULL");
                } else {
                    dataManager.getDraft().setName(draftName);
                    fileController.handleSaveDraftRequest(this, dataManager.getDraft());
                }
            }

        });

        loadDraftButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
        });

        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });

        // DRAFT SCREENS
        playerScreenButton.setOnAction(e -> {
            workspaceScrollPane.setContent(playersScreen.getPlayersScreen());

        });
        fantasyTeamButton.setOnAction(e -> {
            workspaceScrollPane.setContent(fantasyTeamsScreen.getFantasyTeamsScreen());
        });
        fantasyStandingsButton.setOnAction(e -> {
            workspaceScrollPane.setContent(fantasyStandingsScreen.getFantastStandingsScreen());
        });
        draftScreenButton.setOnAction(e -> {
            workspaceScrollPane.setContent(draftSummaryScreen.getDraftSummaryScreen());
        });
        MLBTeamsButton.setOnAction(e -> {
            workspaceScrollPane.setContent(mlbTeamsScreen.getMLBTeamsScreen());
        });
    }

    public static String getPRIMARY_STYLE_SHEET() {
        return PRIMARY_STYLE_SHEET;
    }

    public static String getCLASS_BORDERED_PANE() {
        return CLASS_BORDERED_PANE;
    }

    public static String getCLASS_DRAFT_PANE() {
        return CLASS_DRAFT_PANE;
    }

    public static String getCLASS_HEADING_LABEL() {
        return CLASS_HEADING_LABEL;
    }

    public static String getCLASS_SUBHEADING_LABEL() {
        return CLASS_SUBHEADING_LABEL;
    }

    public static String getCLASS_PROMPT_LABEL() {
        return CLASS_PROMPT_LABEL;
    }

    public static String getEMPTY_TEXT() {
        return EMPTY_TEXT;
    }

    public static int getLARGE_TEXT_FIELD_LENGTH() {
        return LARGE_TEXT_FIELD_LENGTH;
    }

    public static int getSMALL_TEXT_FIELD_LENGTH() {
        return SMALL_TEXT_FIELD_LENGTH;
    }

    public PlayersScreen getPlayersScreen() {
        return playersScreen;
    }

    public ObservableList<Player> getSortedPlayers() {
        return sortedPlayers;
    }

    public DraftFileManager getDraftFileManager() {
        return draftFileManager;
    }

    public DraftExporter getDraftExporter() {
        return draftExporter;
    }

    public DraftEditController getDraftController() {
        return draftController;
    }

    public TeamEditController getTeamController() {
        return teamController;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public Scene getPrimaryScene() {
        return primaryScene;
    }

    public BorderPane getWdkPane() {
        return wdkPane;
    }

    public FlowPane getFileToolbarPane() {
        return fileToolbarPane;
    }

    public Button getNewDraftButton() {
        return newDraftButton;
    }

    public Button getLoadDraftButton() {
        return loadDraftButton;
    }

    public Button getSaveDraftButton() {
        return saveDraftButton;
    }

    public Button getExportDraftButton() {
        return exportDraftButton;
    }

    public Button getExitButton() {
        return exitButton;
    }

    public FlowPane getScreenToolbarPane() {
        return screenToolbarPane;
    }

    public Button getPlayerScreenButton() {
        return playerScreenButton;
    }

    public Button getFantasyTeamButton() {
        return fantasyTeamButton;
    }

    public Button getFantasyStandingsButton() {
        return fantasyStandingsButton;
    }

    public Button getDraftScreenButton() {
        return draftScreenButton;
    }

    public Button getMLBTeamsButton() {
        return MLBTeamsButton;
    }

    public TableView<Player> getPlayerTable() {
        return playerTable;
    }

    public BorderPane getWorkspacePane() {
        return workspacePane;
    }

    public boolean isWorkspaceActivated() {
        return workspaceActivated;
    }

    public ScrollPane getWorkspaceScrollPane() {
        return workspaceScrollPane;
    }

    public VBox getTopWorkspacePane() {
        return topWorkspacePane;
    }

    public Label getDraftHeadingLabel() {
        return draftHeadingLabel;
    }

    public SplitPane getTopWorkSpaceSplitPane() {
        return topWorkSpaceSplitPane;
    }

    public BorderPane getPlayerScreenPane() {
        return playerScreenPane;
    }

    public Button getAddPlayerButton() {
        return addPlayerButton;
    }

    public Button getRemovePlayerButton() {
        return removePlayerButton;
    }

    public Label getPlayerScreenHeading() {
        return playerScreenHeading;
    }

    public BorderPane getFantasyTeamsPane() {
        return fantasyTeamsPane;
    }

    public BorderPane getFantasyStandingsPane() {
        return fantasyStandingsPane;
    }

    public BorderPane getDraftSummaryPane() {
        return draftSummaryPane;
    }

    public BorderPane getMLBTeamsPane() {
        return MLBTeamsPane;
    }

    private void DraftSummary(Stage primaryStage, DraftDataManager dataManager) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setImageLoader(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

}
