package wdk.gui;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import static wdk.WDK_INITConstants.PATH_CSS;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.FantasyTeam;
import wdk.data.Player;

public class FantasyStandings {

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "csb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_TOGGLE_GROUP = "toggle_group";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    Stage primaryStage;
    Draft draft;
    DraftDataManager dataManager;
    Label fantasyStandingsLabel;
    FilteredList<FantasyTeam> filteredTeams;
    SortedList<FantasyTeam> sortedTeams;
    ObservableList<FantasyTeam> fantasyTeams;
    INITComponents componentMaker;

    TableView standingsTable;

    TableColumn teamName;
    TableColumn playersNeeded;
    TableColumn moneyLeft;
    TableColumn moneyPP;
    TableColumn runs;
    TableColumn homeruns;
    TableColumn rbi;
    TableColumn stolenBases;
    TableColumn battingAverage;
    TableColumn wins;
    TableColumn saves;
    TableColumn strikeouts;
    TableColumn era;
    TableColumn whip;
    TableColumn totalPoints;

    BorderPane fantasyStandingsScreen;
    private HBox heading;

    FantasyStandings(Stage initPrimaryStage, DraftDataManager dataManager) {
        this.primaryStage = initPrimaryStage;
        this.dataManager = dataManager;
        this.draft = this.dataManager.getDraft();
        this.fantasyTeams = this.draft.getFantasyTeams();
        filteredTeams = new FilteredList<>(dataManager.getDraft().getFantasyTeams(), p -> true);
        sortedTeams = new SortedList<>(filteredTeams);
        componentMaker = new INITComponents();
        fantasyStandingsScreen = new BorderPane();
        standingsTable = new TableView();
    }

    public void initComponents() {
        fantasyStandingsLabel = componentMaker.initChildLabel("Fantasy Standings Estimates", CLASS_HEADING_LABEL);
        heading = new HBox();
        standingsTable = new TableView();
    }

    void initFantsyStandingsTeam() {
        initComponents();
        initStandingsTable();

        fantasyStandingsScreen.getStyleClass().add(CLASS_BORDERED_PANE);
        heading.getChildren().add(fantasyStandingsLabel);

        fantasyStandingsScreen.setTop(heading);
        fantasyStandingsScreen.setCenter(standingsTable);
    }

    public void initStandingsTable() {
        standingsTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        sortedTeams.comparatorProperty().bind(standingsTable.comparatorProperty());

        teamName = new TableColumn("Team Name");
        playersNeeded = new TableColumn("Players Needed");
        moneyLeft = new TableColumn("$ Left");
        moneyPP = new TableColumn("$ PP");
        runs = new TableColumn("R");
        homeruns = new TableColumn("HR");
        rbi = new TableColumn("RBI");
        stolenBases = new TableColumn("SB");
        battingAverage = new TableColumn("BA");
        wins = new TableColumn("W");
        saves = new TableColumn("SV");
        strikeouts = new TableColumn("K");
        era = new TableColumn("ERA");
        whip = new TableColumn("WHIP");
        totalPoints = new TableColumn("Total Points");
        resizeColumns();

        teamName.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamName"));

        playersNeeded.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("playersNeeded"));
        moneyLeft.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("wallet"));
        moneyPP.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("moneyPP"));
        runs.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("teamRuns"));
        homeruns.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("teamHomeruns"));
        rbi.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("teamRBI"));
        stolenBases.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("teamStolenBases"));
        battingAverage.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Double>("teamBattingAverage"));
        wins.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("teamWins"));
        saves.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("teamSaves"));
        strikeouts.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("teamStrikeouts"));
        era.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("teamERA"));
        whip.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("teamWHIP"));
        totalPoints.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("totalPoints"));

        standingsTable.getColumns().add(teamName);
         standingsTable.getColumns().add(playersNeeded);
        standingsTable.getColumns().addAll(moneyLeft, moneyPP, runs,
                homeruns, rbi, stolenBases, battingAverage, wins,
                saves, strikeouts, era, whip, totalPoints);
        
        standingsTable.setMaxWidth(1000);
        standingsTable.setItems(sortedTeams);
        standingsTable.getSortOrder().add(totalPoints);
    }

    public void resizeColumns() {
        runs.setMinWidth(45);
        runs.setMaxWidth(60);
        homeruns.setMaxWidth(60);
        homeruns.setMinWidth(45);
        rbi.setMaxWidth(60);
        rbi.setMinWidth(45);
        stolenBases.setMaxWidth(60);
        stolenBases.setMinWidth(45);
        battingAverage.setMaxWidth(60);
        battingAverage.setMinWidth(45);
        wins.setMaxWidth(60);
        wins.setMinWidth(45);
        saves.setMaxWidth(60);
        saves.setMinWidth(45);
        strikeouts.setMaxWidth(60);
        strikeouts.setMinWidth(45);
        era.setMaxWidth(60);
        era.setMinWidth(45);
        whip.setMaxWidth(60);
        whip.setMinWidth(45);
    }

    public BorderPane getFantastStandingsScreen() {
        return fantasyStandingsScreen;
    }
}
