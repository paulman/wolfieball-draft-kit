package wdk.gui;

import java.awt.Color;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import wdk.gui.WDK_GUI;
import static wdk.WDK_INITConstants.PATH_CSS;
import static wdk.WDK_INITConstants.PATH_IMAGES;
import wdk.WDK_PropertyType;
import wdk.controller.DraftEditController;
import wdk.controller.PlayerController;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.file.ImageLoader;
import wdk.gui.LabelMaker;

public class PlayersScreen {

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "csb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_TABLE_VIEW = "table_view";
    static final String CLASS_TEXT_LABE = "text_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_TOGGLE_GROUP = "toggle_group";
    static final String CLASS_HEADING_BOX = "heading_box";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    MessageDialog messageDialog;
    Stage primaryStage;
    Draft draft;
    DraftDataManager dataManager;
    BorderPane playerScreenPane;
    Button addPlayerButton;
    Button removePlayerButton;
    Label playerScreenHeading;
    TableView<Player> playerTable;
    PlayerController playerController;
    Player currentPlayer;
    ImageLoader imageLoader;
    DraftEditController draftController;

    FilteredList<Player> filteredPlayers;
    SortedList<Player> sortedPlayers;

    // FILTERS
    String positionFilter = "";
    String nameFilter = "";

    // LABEL/BUTTON MAKER
    INITComponents componentMaker;

    TextField searchBox;
    Label searchLabel;

    // PLAYER SCREEN BOXES
    VBox heading;
    VBox playerData;
    HBox sort;
    HBox edit;

    // RADIO BUTTON GROUP
    RadioButton all;
    RadioButton c;
    RadioButton fBase;
    RadioButton ci;
    RadioButton tBase;
    RadioButton mi;
    RadioButton ss;
    RadioButton oF;
    RadioButton u;
    RadioButton p;
    ToggleGroup group;

    // TABLE COLUMNS
    TableColumn position;
    TableColumn notes;
    TableColumn fName;
    TableColumn lName;
    TableColumn value;
    TableColumn team;
    TableColumn yob;
    TableColumn<Player, Integer> r_w;
    TableColumn<Player, Integer> hr_sv;
    TableColumn<Player, Integer> rbi_k;
    TableColumn<Player, Double> sb_era;
    TableColumn<Player, Double> ba_whip;

    public PlayersScreen(Stage initPrimaryStage, DraftDataManager dataManager, MessageDialog messageDialog, ImageLoader imageLoader, DraftEditController draftController) {
        this.primaryStage = initPrimaryStage;
        this.dataManager = dataManager;
        this.messageDialog = messageDialog;
        this.imageLoader = imageLoader;
        this.draftController = draftController;
        draft = this.dataManager.getDraft();
        System.out.println(draft.getPlayerPool() == null);
        filteredPlayers = new FilteredList<>(draft.getPlayerPool(), p -> true);
        sortedPlayers = new SortedList<>(filteredPlayers);
        componentMaker = new INITComponents();
        playerController = new PlayerController(this.primaryStage, this.dataManager, this.messageDialog, this.imageLoader, this.draftController);
    }

    public void initComponents() {
        playerScreenHeading = componentMaker.initChildLabel("Available Players", CLASS_HEADING_LABEL);
        searchLabel = componentMaker.initChildLabel("Search", CLASS_SUBHEADING_LABEL);
        addPlayerButton = componentMaker.initChildButton(playerScreenPane, WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_PLAYER_TOOLTIP, false);
        removePlayerButton = componentMaker.initChildButton(playerScreenPane, WDK_PropertyType.REMOVE_ICON, WDK_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
    }

    public void initToggleGroup() {
        sort.getChildren().add(all);
        sort.getChildren().add(c);
        sort.getChildren().add(fBase);
        sort.getChildren().add(ci);
        sort.getChildren().add(tBase);
        sort.getChildren().add(mi);
        sort.getChildren().add(ss);
        sort.getChildren().add(oF);
        sort.getChildren().add(u);
        sort.getChildren().add(p);
        sort.getStyleClass().add(CLASS_TOGGLE_GROUP);
    }

    public void initPlayersScreen() {
        // GENERAL DECLARATIONS AND INITIALIZATIONS
        playerScreenPane = new BorderPane();
        playerData = new VBox();
        sort = new HBox();
        heading = new VBox();
        edit = new HBox();
        searchBox = new TextField();
        HBox.setHgrow(searchBox, Priority.ALWAYS);
        playerTable = new TableView<Player>();

        initComponents();
        initPlayerTable();
        initRadioButtons();
        initEventHandlers();

        playerScreenPane.getStyleClass().add(CLASS_BORDERED_PANE);
        //playerTable.getStyleClass().add(CLASS_TABLE_VIEW);
        playerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        edit.getChildren().add(addPlayerButton);
        edit.getChildren().add(removePlayerButton);
        edit.getChildren().add(searchLabel);
        edit.getChildren().add(searchBox);

        heading.getChildren().add(playerScreenHeading);
        heading.getChildren().add(edit);
        heading.getStyleClass().add(CLASS_HEADING_BOX);

        playerData.getChildren().add(sort);
        playerData.getChildren().add(playerTable);

        playerScreenPane.setTop(heading);
        playerScreenPane.setCenter(playerData);
    }

    private void initRadioButtons() {
        group = new ToggleGroup();
        all = new RadioButton("All");
        c = new RadioButton("C");
        fBase = new RadioButton("1B");
        ci = new RadioButton("CI"); //corner infielder
        tBase = new RadioButton("3B");
        mi = new RadioButton("MI"); //middle infield
        ss = new RadioButton("SS");
        oF = new RadioButton("OF");
        u = new RadioButton("U");
        p = new RadioButton("P");

        all.setToggleGroup(group);
        c.setToggleGroup(group);
        fBase.setToggleGroup(group);
        ci.setToggleGroup(group);
        tBase.setToggleGroup(group);
        mi.setToggleGroup(group);
        ss.setToggleGroup(group);
        oF.setToggleGroup(group);
        u.setToggleGroup(group);
        p.setToggleGroup(group);
        initToggleGroup();
    }

    public void setTableColNames(String playerType) {
        switch (playerType) {
            case "pitcher":
                r_w.setText("W");
                hr_sv.setText("SV");
                rbi_k.setText("K");
                sb_era.setText("ERA");
                ba_whip.setText("WHIP");
                break;
            case "hitter":
                r_w.setText("R");
                hr_sv.setText("HR");
                rbi_k.setText("RBI");
                sb_era.setText("SB");
                ba_whip.setText("BA");
                break;
            case "player":
                r_w.setText("R/W");
                hr_sv.setText("HR/SV");
                rbi_k.setText("RBI/K");
                sb_era.setText("SB/ERA");
                ba_whip.setText("BA/WHIP");
                break;
        }
    }

    public void initPlayerTable() {
        sortedPlayers.comparatorProperty().bind(playerTable.comparatorProperty());
        fName = new TableColumn("First");
        lName = new TableColumn("Last");
        team = new TableColumn("Pro Team");
        position = new TableColumn("Positions");
        yob = new TableColumn("Year of Birth");
        r_w = new TableColumn<Player, Integer>("R/W");
        hr_sv = new TableColumn<Player, Integer>("HR/SV");
        rbi_k = new TableColumn<Player, Integer>("RBI/K");
        sb_era = new TableColumn<Player, Double>("SB/ERA");
        ba_whip = new TableColumn<Player, Double>("BA/WHIP");
        value = new TableColumn("Estimated Value");

        notes = new TableColumn("Notes");
        notes.setEditable(true);
        notes.setCellFactory(TextFieldTableCell.forTableColumn());

        fName.setCellValueFactory(new PropertyValueFactory<String, String>("fName"));
        lName.setCellValueFactory(new PropertyValueFactory<String, String>("lName"));
        team.setCellValueFactory(new PropertyValueFactory<String, String>("proTeam"));
        yob.setCellValueFactory(new PropertyValueFactory<String, String>("YOB"));
        position.setCellValueFactory(new PropertyValueFactory<String, String>("positions"));
        
        value.setCellValueFactory(new PropertyValueFactory<Player, Integer>("estimatedValue"));
        notes.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));

        r_w.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                if (p.getValue() instanceof Pitcher) {
                    return new ReadOnlyObjectWrapper<>(((Pitcher) p.getValue()).getWins());
                } else if (p.getValue() instanceof Hitter) {
                    Integer runs = ((Hitter) p.getValue()).getRuns();
                    return new ReadOnlyObjectWrapper<Integer>(runs);
                }
                return null;
            }
        });

        hr_sv.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                if (p.getValue() instanceof Pitcher) {
                    int sv = ((Pitcher) p.getValue()).getSaves();
                    return new ReadOnlyObjectWrapper<Integer>(sv);
                } else if (p.getValue() instanceof Hitter) {
                    Integer hr = ((Hitter) p.getValue()).getHomeRuns();
                    return new ReadOnlyObjectWrapper<Integer>(hr);
                }
                return null;
            }
        });

        rbi_k.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {

            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Player, Integer> p) {
                if (p.getValue() instanceof Pitcher) {
                    int k = ((Pitcher) p.getValue()).getStrikeOuts();

                    return new ReadOnlyObjectWrapper<Integer>(k);
                } else if (p.getValue() instanceof Hitter) {
                    Integer rbi = ((Hitter) p.getValue()).getRbi();
                    return new ReadOnlyObjectWrapper<Integer>(rbi);
                }
                return null;
            }
        });

        sb_era.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {
            public ObservableValue<Double> call(TableColumn.CellDataFeatures<Player, Double> p) {
                if (p.getValue() instanceof Pitcher) {
                    Double era = ((Pitcher) p.getValue()).getEra();

                    return new ReadOnlyObjectWrapper<Double>(era);
                } else if (p.getValue() instanceof Hitter) {
                    double sb = ((Hitter) p.getValue()).getStolenBases();
                    sb = Math.round(sb);
                    return new ReadOnlyObjectWrapper<Double>(sb);
                }
                return null;
            }
        });

        ba_whip.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {
            public ObservableValue<Double> call(TableColumn.CellDataFeatures<Player, Double> p) {
                if (p.getValue() instanceof Pitcher) {
                    Double whip = ((Pitcher) p.getValue()).getWhip();
                    return new ReadOnlyObjectWrapper<Double>(whip);
                } else if (p.getValue() instanceof Hitter) {
                    Double ba = ((Hitter) p.getValue()).getBa();
                    return new ReadOnlyObjectWrapper<Double>(ba);
                }
                return null;
            }
        });

        playerTable.getColumns().add(fName);
        playerTable.getColumns().add(lName);
        playerTable.getColumns().add(team);
        playerTable.getColumns().add(position);
        playerTable.getColumns().add(yob);
        playerTable.getColumns().add(r_w);
        playerTable.getColumns().add(hr_sv);
        playerTable.getColumns().add(rbi_k);
        playerTable.getColumns().add(sb_era);
        playerTable.getColumns().add(ba_whip);
        playerTable.getColumns().add(value);
        playerTable.getColumns().add(notes);
        playerTable.setItems(sortedPlayers);
        playerTable.setEditable(true);
        playerTable.getSortOrder().add(lName);
    }

    private boolean containsName(Player player) {
        if (player.getFName().toLowerCase().contains(nameFilter)) {
            return true;
        } else if (player.getLName().toLowerCase().contains(nameFilter)) {
            return true;
        }
        return false;
    }

    private boolean containsPosition(Player player) {
        if (player.getPositions().contains(positionFilter) && containsName(player)) {
            return true;
        }
        return false;
    }

    public void setFreeAgents() {
//        filteredPlayers.setPredicate(player -> {
//            for (int i = 0; i < draft.getPlayerPool().size(); i++) {
//                if (draft.getPlayersPool)
//            }
//            if (player.isFreeAgent())
//                return true;
//        });
    }

    private void initEventHandlers() {
        // FOR RADIO BUTTONS
        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                if (group.getSelectedToggle() != null) {
                    filteredPlayers.setPredicate(player -> {
                        RadioButton chk = (RadioButton) new_toggle.getToggleGroup().getSelectedToggle();
                        positionFilter = chk.getText();
                        if (positionFilter.equals("All") && player.isFreeAgent()) {
                            setTableColNames("player");
                            return true;
                        } else if (positionFilter.equals("P")) {
                            setTableColNames("pitcher");
                        } else {
                            setTableColNames("hitter");
                        }
                        if (player.getPositions().contains(positionFilter) && containsName(player) && player.isFreeAgent()) {
                            return true;
                        }
                        return false;
                    });
                }
            }
        });
        searchBox.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredPlayers.setPredicate(player -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                nameFilter = newValue.toLowerCase();
                if (containsPosition(player) && player.getFName().toLowerCase().contains(nameFilter) && player.isFreeAgent()) {
                    return true;
                } else if (containsPosition(player) && player.getLName().toLowerCase().contains(nameFilter) && player.isFreeAgent()) {
                    return true;
                }
                return false;
            });
        });
        // PLAYER TABLE 
        playerTable.setOnMouseClicked(e -> {
            currentPlayer = playerTable.getSelectionModel().getSelectedItem();
            if (e.getClickCount() == 2) {
                System.out.println("DOUBLE CLICK");
                System.out.println(currentPlayer.getLName());
                playerController.handleEditPlayerRequest(currentPlayer);
            }
            removePlayerButton.setDisable(false);
        });

        removePlayerButton.setOnAction(e -> {
            if (draft.getPlayerPool().remove(currentPlayer)) {
//                draftController.rankPlayers();
//                draftController.rankTeams();
                System.out.println("Successfully removed.");
            } else {
                System.out.println("Something went wrong.");
            }
        });

        addPlayerButton.setOnAction(e -> {
            playerController.handleAddPlayerRequest();
        });
    }

    public BorderPane getPlayersScreen() {
        return playerScreenPane;
    }
}
