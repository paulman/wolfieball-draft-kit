package wdk.gui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Task;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import static wdk.WDK_INITConstants.PATH_CSS;
import wdk.WDK_PropertyType;
import wdk.controller.DraftEditController;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.FantasyTeam;
import wdk.data.Player;
import wdk.error.CantAffordException;
import static wdk.gui.FantasyStandings.CLASS_HEADING_LABEL;

public class DraftSummary {

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_TOGGLE_GROUP = "toggle_group";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    Player dummy = new Player();

    Stage primaryStage;
    Draft draft;
    DraftDataManager dataManager;
    Label draftSummaryLabel;
    FilteredList<Player> filteredPlayers;
    SortedList<Player> sortedPlayers;
    INITComponents componentMaker;
    TableView draftTable;
    MessageDialog messageDialog;
    DraftEditController draftController;

    TableColumn pickNum;
    TableColumn firstName;
    TableColumn lastName;
    TableColumn teamName;
    TableColumn contract;
    TableColumn salary;

    Button selectPlayerButton;
    Button startAutoDraftButton;
    Button pauseAutoDraftButton;

    HBox buttonBox;
    VBox dataBox;

    BorderPane draftSummaryScreen;
    private HBox heading;

    DraftSummary(Stage initPrimaryStage, DraftDataManager dataManager, MessageDialog messageDialog, DraftEditController draftController) {
        this.primaryStage = initPrimaryStage;
        this.dataManager = dataManager;
        this.draft = dataManager.getDraft();
        this.messageDialog = messageDialog;
        this.draftController = draftController;
        filteredPlayers = new FilteredList<>(dataManager.getDraft().getPlayerPool(), p -> true);
        sortedPlayers = new SortedList<>(filteredPlayers);
        componentMaker = new INITComponents();
    }

    public void initComponents() {
        draftSummaryScreen = new BorderPane();
        heading = new HBox();
        buttonBox = new HBox();
        dataBox = new VBox();

        draftTable = new TableView();
        draftSummaryLabel = componentMaker.initChildLabel("Draft Summary", CLASS_HEADING_LABEL);
        selectPlayerButton = componentMaker.initChildButton(buttonBox, "selectPlayer.png", "Select a Player", false);
        startAutoDraftButton = componentMaker.initChildButton(buttonBox, "startDraft.png", "Start Auto-Draft", false);
        pauseAutoDraftButton = componentMaker.initChildButton(buttonBox, "pauseDraft.png", "Pause Auto-Draft", true);
    }

    void initDraftSummaryScreen() {
        initComponents();
        initEventHandlers();
        initDraftTable();
        draftSummaryScreen.getStyleClass().add(CLASS_BORDERED_PANE);

        heading.getChildren().add(draftSummaryLabel);
        dataBox.getChildren().add(heading);
        dataBox.getChildren().add(buttonBox);
        dataBox.getChildren().add(draftTable);

        draftSummaryScreen.setCenter(dataBox);

    }

    public void initDraftTable() {
        draftTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        pickNum = new TableColumn("Pick #");
        firstName = new TableColumn("First");
        lastName = new TableColumn("Last");
        teamName = new TableColumn("Team");
        contract = new TableColumn("Contract");
        salary = new TableColumn("Salary ($)");
        setSortable();

        // DRAFT PICK NUM 
        pickNum.setCellValueFactory(new Callback<CellDataFeatures<Player, Player>, ObservableValue<Player>>() {
            @Override
            public ObservableValue<Player> call(CellDataFeatures<Player, Player> p) {
                return new ReadOnlyObjectWrapper(p.getValue());
            }
        });
        pickNum.setCellFactory(new Callback<TableColumn<Player, Player>, TableCell<Player, Player>>() {
            @Override
            public TableCell<Player, Player> call(TableColumn<Player, Player> param) {
                return new TableCell<Player, Player>() {
                    @Override
                    protected void updateItem(Player item, boolean empty) {
                        super.updateItem(item, empty);

                        if (this.getTableRow() != null && item != null) {
                            setText((this.getTableRow().getIndex() + 1) + "");
                        } else {
                            setText("");
                        }
                    }
                };
            }
        });

        firstName.setCellValueFactory(new PropertyValueFactory<Player, String>("fName"));
        lastName.setCellValueFactory(new PropertyValueFactory<Player, String>("lName"));
        teamName.setCellValueFactory(new PropertyValueFactory<Player, String>("fanTeamName"));
        contract.setCellValueFactory(new PropertyValueFactory<Player, String>("contract"));
        salary.setCellValueFactory(new PropertyValueFactory<Player, String>("salaryString"));

        draftTable.getColumns().add(pickNum);
        draftTable.getColumns().add(firstName);
        draftTable.getColumns().add(lastName);
        draftTable.getColumns().add(teamName);
        draftTable.getColumns().add(contract);
        draftTable.getColumns().add(salary);

        draftTable.setMaxWidth(800);
        draftTable.setItems(draft.getDraftOrder());
    }

    public boolean addPlayer(FantasyTeam ft) {
        int i = (int) (Math.random() * draft.getPlayerPool().size() - 1);
        Player p = draft.getPlayerPool().get(i);

        for (int x = 0; x < p.getPositionsList().size(); x++) {
            if (ft.getPositionsNeeded().contains(p.getPositionsList().get(x))) {
                String pos = p.getPositionsList().get(x).toString();
                p.setContract("S2");
                p.setFanTeamName(ft.getTeamName());
                p.setFreeAgent(false);
                p.setSalary(1);
                p.setFixedPosition(pos);
                draft.getDraftOrder().add(p);
                try {
                    ft.addPlayer(draft.getPlayerPool().remove(draft.getPlayerPool().indexOf(p)));
                } catch (CantAffordException ex) {

                }
                draftController.rankTeams();
                draftController.rankPlayers();
                return true;
            }
        }
        return false;
    }

    public boolean addToTaxiSquad(FantasyTeam ft) {
        int i = (int) (Math.random() * draft.getPlayerPool().size() - 1);
        Player p = draft.getPlayerPool().get(i);
        p.setContract("X");
        p.setFanTeamName(ft.getTeamName());
        p.setFreeAgent(false);
        p.setSalary(1);
        p.setFixedPosition(p.getPositionsList().get(0).toString());
        try {
            ft.addPlayer(draft.getPlayerPool().remove(draft.getPlayerPool().indexOf(p)));
        } catch (CantAffordException ex) {
            Logger.getLogger(DraftSummary.class.getName()).log(Level.SEVERE, null, ex);
        }
        draftController.rankTeams();
        draftController.rankPlayers();
        return true;
    }

    public boolean selectPlayer() {

        if (areAllTeamsFull() && isAllTaxiSquadsFull()) {
            messageDialog.show("All Teams are full");
            return false;
        }
        boolean addedPlayer = false;
        boolean full = false;
        int playersNeeded = 0;
        int count = 0;

        if (!areAllTeamsFull()) {
            while (!addedPlayer) {
                for (FantasyTeam ft : draft.getFantasyTeams()) {
                    if (!isTeamFull(ft)) {
                        if (addPlayer(ft)) {
                            addedPlayer = true;
                        }
                        if (addedPlayer) {
                            break;
                        }
                    }
                }
            }
        } else if (!isAllTaxiSquadsFull()) {
            while (!addedPlayer) {
                for (FantasyTeam ft : draft.getFantasyTeams()) {
                    if (!isTaxiSquadFull(ft)) {
                        if (addToTaxiSquad(ft)) {
                            addedPlayer = true;
                        }
                        if (addedPlayer) {
                            break;
                        }
                    }
                }
            }
        }
        return true;
    }

    public boolean isTeamFull(FantasyTeam ft) {
        return ft.getPlayersNeeded() == 0;
    }

    public boolean areAllTeamsFull() {
        for (FantasyTeam ft : draft.getFantasyTeams()) {
            if (!isTeamFull(ft)) {
                return false;
            }
        }
        return true;
    }

    public boolean isTaxiSquadFull(FantasyTeam ft) {
        return ft.getTaxiSquad().size() == 8;
    }

    public boolean isAllTaxiSquadsFull() {
        for (FantasyTeam ft : draft.getFantasyTeams()) {
            if (!isTaxiSquadFull(ft)) {
                return false;
            }
        }
        return true;
    }

    public void initEventHandlers() {
        selectPlayerButton.setOnAction(e -> {
            selectPlayer();
        });
        startAutoDraftButton.setOnAction(e -> {
            startAutoDraft();
        });
    }

    public BorderPane getDraftSummaryScreen() {
        return draftSummaryScreen;
    }

    public void setSortable() {
        pickNum.setSortable(false);
        firstName.setSortable(false);
        lastName.setSortable(false);
        contract.setSortable(false);
        salary.setSortable(false);

        pickNum.setResizable(false);
        pickNum.setMinWidth(60);
        pickNum.setMaxWidth(60);
    }

    public boolean areAllFull() {
        return areAllTeamsFull() && isAllTaxiSquadsFull();
    }

    public void startAutoDraft() {
        pauseAutoDraftButton.setDisable(false);
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {

                while (!areAllFull()) {
                    // WE'RE SLEEPING FIRST JUST TO LET THE FIRST MESSAGE SHOW
                    Thread.sleep(250);

                    // UPDATE ANY PROGRESS DISPLAY
                    Platform.runLater(new Runnable() {

                        public void run() {
                            System.out.println("RUN!!!");
                            selectPlayer();
                            draftController.rankTeams();
                            draftController.rankPlayers();
                        }
                    });
                };
                return null;
            }
        };

        Thread thread = new Thread(task);
        thread.start();
        pauseAutoDraftButton.setOnAction(e -> {
            thread.stop();
        });
    }
}
