package wdk.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wdk.data.FantasyTeam;
import static wdk.gui.FantasyTeamsScreen.PRIMARY_STYLE_SHEET;

public class FantasyTeamDialog extends Stage {

    FantasyTeam newTeam;
    MessageDialog messageDialog;

    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    TextField ownerTextField;
    Label ownerLabel;
    TextField nameTextField;
    Label nameLabel;
    Button completeButton;
    Button cancelButton;

    String selection;

    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String NAME_PROMPT = "Name: ";
    public static final String OWNER_PROMPT = "Owner: ";
    public static final String CLASS_DIALOG_HEADING = "dialog_heading";
    public static final String TEAM_HEADING = "Fantasy Team Details";
    public static final String CLASS_HEADING_LABEL = "heading_label";
    public static final String CLASS_PROMPT_LABEL = "prompt_label";
    public static final String ADD_TEAM_TITLE = "Add New Fantasy Team";
    public static final String EDIT_TEAM_TITLE = "Edit Fantasy Team";

    public FantasyTeamDialog(Stage initPrimaryStage, MessageDialog messageDialog) {//, MessageDialog messageDialog) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(initPrimaryStage);
        this.messageDialog = messageDialog;
        initComponents();

        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        headingLabel = new Label(TEAM_HEADING);
        headingLabel.getStyleClass().add(CLASS_DIALOG_HEADING);

        nameLabel = new Label(NAME_PROMPT);
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);

        ownerLabel = new Label(OWNER_PROMPT);
        ownerLabel.getStyleClass().add(CLASS_PROMPT_LABEL);

        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(nameLabel, 0, 1, 1, 1);
        gridPane.add(nameTextField, 1, 1, 1, 1);
        gridPane.add(ownerLabel, 0, 3, 1, 1);
        gridPane.add(ownerTextField, 1, 3, 1, 1);
        gridPane.add(completeButton, 0, 4, 1, 1);
        gridPane.add(cancelButton, 1, 4, 1, 1);

        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
        initEventHandlers();
    }

    public void initComponents() {
        nameTextField = new TextField();
        ownerTextField = new TextField();
        gridPane = new GridPane();
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
    }

    public void initEventHandlers() {
        completeButton.setOnAction(e -> {
            boolean errorFree = true;
            if (nameTextField.getText().length() <1 || ownerTextField.getText().length() <1) {
                errorFree = false;
                messageDialog.show("Please fill complete all fields");
            } else {
                errorFree = true;
                selection = completeButton.getText();
                newTeam.setTeamName(nameTextField.getText());
                newTeam.setOwnerName(ownerTextField.getText());

            this.hide();
            }
            
        });
        cancelButton.setOnAction(e -> {
            selection = cancelButton.getText();

            this.hide();
        });

    }

    public FantasyTeam getTeam() {
        return newTeam;
    }

    public FantasyTeam showAddTeamDialog() {
        setTitle(ADD_TEAM_TITLE);

        newTeam = new FantasyTeam();

        nameTextField.setText(newTeam.getTeamName());
        ownerTextField.setText(newTeam.getOwnerName());

        this.showAndWait();
//        System.out.println("team: " + newTeam.getTeamName());
//        System.out.println("owner: " + newTeam.getOwnerName());

        return newTeam;
    }

    public void showEditTeamDialog(FantasyTeam editTeam) {
        setTitle(EDIT_TEAM_TITLE);

        newTeam = new FantasyTeam();

        
        newTeam.setTeamName(editTeam.getTeamName());
        newTeam.setOwnerName(editTeam.getOwnerName());

        loadGUIData();
        this.showAndWait();
    }

    public void loadGUIData() {
        nameTextField.setText(newTeam.getTeamName());
        ownerTextField.setText(newTeam.getOwnerName());
    }

    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
}
