/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk;

import properties_manager.PropertiesManager;
import xml_utilities.InvalidXMLFileFormatException;
import wdk.gui.WDK_GUI;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import static wdk.WDK_INITConstants.JSON_FILE_PATH_HITTERS;
import static wdk.WDK_INITConstants.JSON_FILE_PATH_PITCHERS;
import static wdk.WDK_INITConstants.PATH_BASE;
import static wdk.WDK_INITConstants.PATH_DATA;
import static wdk.WDK_INITConstants.PATH_SITES;
import static wdk.WDK_INITConstants.PROPERTIES_FILE_NAME;
import static wdk.WDK_INITConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static wdk.WDK_PropertyType.PROP_APP_TITLE;
import wdk.data.DraftDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.data.Positions.*;
import static wdk.data.Positions.CATCHER;
import static wdk.data.Positions.PITCHER;
import wdk.error.ErrorHandler;
import wdk.file.DraftExporter;
import wdk.file.ImageLoader;
import wdk.file.JsonDraftFileManager;

public class WolfieballDraftKit extends Application {

    WDK_GUI gui;
    DraftDataManager draftManager;
    ImageLoader imageLoader;

    @Override
    public void start(Stage primaryStage) {
        
        // Implement error handling later
        
//        ErrorHandler eH = ErrorHandler.getErrorHandler();
//        System.out.println("Made it: after eH /(/)");

        //eH.initMessageDialog(primaryStage);
        
        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(PROP_APP_TITLE);
            try {
                
                JsonDraftFileManager jsonFileManager = new JsonDraftFileManager();
                //DraftExporter exporter = new DraftExporter(PATH_BASE, PATH_SITES);
            
                ArrayList<Hitter> hitters = jsonFileManager.loadHitters(JSON_FILE_PATH_HITTERS);
                ArrayList<Pitcher> pitchers = jsonFileManager.loadPitchers(JSON_FILE_PATH_PITCHERS);
                ArrayList<Player> players = new ArrayList<Player>();
                for (int i = 0; i < hitters.size(); i++) {
                    players.add(hitters.get(i));
                }
                 for (int i = 0; i < pitchers.size(); i++) {
                    players.add(pitchers.get(i));
                }
                 
                imageLoader = new ImageLoader(players);
                draftManager = new DraftDataManager(gui);
                draftManager.getDraft().setPlayerPool(players);
               
                gui = new WDK_GUI(primaryStage);
                gui.setDataManager(draftManager);
                gui.setImageLoader(imageLoader);
                gui.setDraftFileManager(jsonFileManager);
                
                //gui.setDraftFileManager(jsonFileManager);
                //gui.setDraftExporter(exporter);
                
                gui.initGUI(appTitle, players);  
            
            } catch (IOException ioe) {
//                eH = ErrorHandler.getErrorHandler();
//                eH.handlePropertiesFileError();
            }
            
            //gui.setDraftFileManager(jsonFileManager);
            // gui.setSiteExporter(exporter);

            // DraftDataManager dataManager = new DraftDataManager(gui);
            // gui.setDataManager(dataManager);

        }

    }

    public boolean loadProperties() {
        try {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
        } catch (InvalidXMLFileFormatException ixmlffe) {
            System.out.println("Error");
//            ErrorHandler eH = ErrorHandler.getErrorHandler();
//            eH.handlePropertiesFileError();
            return false;
        }
    }

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        launch(args);
    }

}
