package wdk.controller;

import javafx.collections.ObservableList;
import javafx.stage.Stage;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.data.FantasyTeam;
import wdk.file.ImageLoader;
import wdk.gui.MessageDialog;
import wdk.gui.PlayerDialog;

public class PlayerController {

    PlayerDialog playerDialog;
    MessageDialog messageDialog;
    DraftDataManager ddm;
    Draft draft;
    ObservableList<FantasyTeam> fantasyTeams;
    Stage primaryStage;
    ImageLoader imageLoader;
    DraftEditController draftController;

    public PlayerController(Stage initPrimaryStage, DraftDataManager ddm, MessageDialog messageDialog, ImageLoader imageLoader, DraftEditController draftController) {
        primaryStage = initPrimaryStage;
        this.messageDialog = messageDialog;
        this.ddm = ddm;
        this.draftController = draftController;
        draft = ddm.getDraft();
        playerDialog = new PlayerDialog(primaryStage, this.messageDialog, draft);
        this.imageLoader = imageLoader;
    }

    public void handleAddPlayerRequest() {
        playerDialog = new PlayerDialog(primaryStage, this.messageDialog, draft);
        playerDialog.showAddPlayerDialog();

        if (playerDialog.wasCompleteSelected()) {
                        Player newPlayer = playerDialog.getPlayer();
            draft.getPlayerPool().add(newPlayer);
            draft.getAllPlayers().add(newPlayer);

            draftController.rankPlayers();

        } else {
            // USER PRESSED CANCEL, DO NOTHING
        }

    }

    public void handleEditPlayerRequest(Player currentPlayer) {
        playerDialog = new PlayerDialog(primaryStage, this.messageDialog, draft);
        playerDialog.showEditPlayerDialog(currentPlayer, imageLoader);

        if (playerDialog.wasCompleteSelected()) {
            draftController.rankTeams();
            draftController.rankPlayers();
            //currentPlayer.setTeamName(tempTeam.getTeamName());
            //currentPlayer.setOwnerName(tempTeam.getOwnerName());
        } else {
            // CANCEL PRESSED
        }
    }

    public void handleRemovePlayerRequest(Player playerToRemove) {

        draftController.rankPlayers();
//        if (draft.getFantasyTeams().remove(playerToRemove)){
//            System.out.println("Removed successfully");
//        } else {
//            System.out.println("something went wrong");
//        }
    }

}
