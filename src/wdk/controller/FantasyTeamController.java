package wdk.controller;

import javafx.collections.ObservableList;
import javafx.stage.Stage;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.FantasyTeam;
import wdk.gui.FantasyTeamDialog;
import wdk.gui.FantasyTeamsScreen;
import wdk.gui.MessageDialog;

public class FantasyTeamController {

    FantasyTeamDialog fanTeamDialog;
    DraftDataManager ddm;
    Draft draft;
    ObservableList<FantasyTeam> fantasyTeams;
    Stage primaryStage;
    MessageDialog messageDialog;
    DraftEditController draftController;

    public FantasyTeamController(Stage initPrimaryStage, DraftDataManager ddm, MessageDialog messageDialog, DraftEditController draftController) {
        primaryStage = initPrimaryStage;
        this.ddm = ddm;
        draft = ddm.getDraft();
        this.messageDialog = messageDialog;
        this.draftController = draftController;
        fanTeamDialog = new FantasyTeamDialog(primaryStage, messageDialog);

    }

    public void handleAddTeamRequest() {
        fanTeamDialog.showAddTeamDialog();

        if (fanTeamDialog.wasCompleteSelected()) {
            FantasyTeam newTeam = fanTeamDialog.getTeam();

            draft.getFantasyTeams().add(newTeam);
            draftController.rankTeams();
            draftController.rankPlayers();
        } else {
            // USER PRESSED CANCEL, DO NOTHING
        }

    }

    public void handleEditTeamRequest(FantasyTeam currentTeam) {
        fanTeamDialog.showEditTeamDialog(currentTeam);
        FantasyTeam tempTeam = fanTeamDialog.getTeam();
        if (fanTeamDialog.wasCompleteSelected()) {
            currentTeam.setTeamName(tempTeam.getTeamName());
            currentTeam.setOwnerName(tempTeam.getOwnerName());
        } else {
            // CANCEL PRESSED
        }
    }

    public void handleRemoveTeamRequest(FantasyTeam teamToRemove) {
        if (draft.getFantasyTeams().remove(teamToRemove)) {
            System.out.println("Removed successfully");
            draftController.rankTeams();
            draftController.rankPlayers();
        } else {
            System.out.println("something went wrong");
        }
    }
}
