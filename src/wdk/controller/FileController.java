/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import static wdk.WDK_PropertyType.DRAFT_SAVED_MESSAGE;
import static wdk.WDK_PropertyType.NEW_DRAFT_CREATED_MESSAGE;
import static wdk.WDK_PropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static wdk.WDK_INITConstants.PATH_DRAFTS;

import properties_manager.PropertiesManager;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.error.ErrorHandler;
import wdk.file.DraftExporter;
import wdk.file.DraftFileManager;
import wdk.gui.MessageDialog;
import wdk.gui.WDK_GUI;
import wdk.gui.YesNoCancelDialog;

/**
 *
 * @author PaulSack
 */
public class FileController {

    private boolean saved;

    private DraftFileManager draftIO;

    ErrorHandler errorHandler;

    MessageDialog messageDialog;

    YesNoCancelDialog yesNoCancelDialog;

    DraftExporter exporter;

    PropertiesManager properties;

    public FileController(MessageDialog initMessageDialog,
            YesNoCancelDialog initYesNoCancelDialog, DraftFileManager initDraftIO) {
        saved = true;

        draftIO = initDraftIO;
//        exporter = initExporter;

        //errorHandler = ErrorHandler.getErrorHandler();

        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
        properties = PropertiesManager.getPropertiesManager();
    }

    public FileController() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void markAsEditied(WDK_GUI gui) {
        saved = false;

        gui.updateToolbarControls(saved);
    }

    public void handleNewDraftRequest(WDK_GUI gui) {
        try {
            boolean continueToMakeNew = true;
            if (!saved) {
                continueToMakeNew = promptToSave(gui);
            }
            if (continueToMakeNew) {
                gui.getDataManager().reset(gui);
//                DraftDataManager dataManager = gui.getDataManager();
//                dataManager.reset();
//                saved = false;

                gui.updateToolbarControls(saved);
                gui.initScreens();
                gui.activateWorkspace();
                messageDialog.show(properties.getProperty(NEW_DRAFT_CREATED_MESSAGE));
            }
        } catch (IOException ioe) {
            //errorHandler.handleNewDraftError();
        }

    }

    public void handleLoadDraftRequest(WDK_GUI gui) {
        try {
            boolean continueToOpen = true;
            if (!saved) {
                continueToOpen = promptToSave(gui);
            }

            if (continueToOpen) {
                promptToOpen(gui);
            }
        } catch (IOException ioe) {
            System.out.println("_____COULDN'T LOAD_____");
            //errorHandler.handleLoadDraftError();
        }
    }

    public void handleSaveDraftRequest(WDK_GUI gui, Draft draftToSave) {
        try {
            draftToSave.toString();
            draftIO.saveDraft(draftToSave);

            saved = true;

            messageDialog.show(properties.getProperty(DRAFT_SAVED_MESSAGE));

            gui.updateToolbarControls(saved);
        } catch (IOException ioe) {
            System.out.println("___SAVING WENT WRONG___");
            
            //errorHandler.handleSaveDraftError();
        }
    }

    public void handleExportDraftRequest(WDK_GUI gui) {
        DraftDataManager dataManager = gui.getDataManager();
        Draft draftTpExport = dataManager.getDraft();

    }

    public void handleExitRequest(WDK_GUI gui) {
        //try {
            boolean continueToExit = true;
//            if (!saved) {
//                continueToExit = promptToSave(gui);
//            }
            
            if (continueToExit) {
                System.exit(0);
            }
//        } catch (IOException ioe) {
////            ErrorHandler eH = ErrorHandler.getErrorHandler();
////            eH.handleExitError();
//        }
    }
    private boolean promptToSave(WDK_GUI gui) throws IOException {
        yesNoCancelDialog.show(properties.getProperty(SAVE_UNSAVED_WORK_MESSAGE));

        String selection = yesNoCancelDialog.getSelection();

        if (selection.equals(YesNoCancelDialog.YES)) {
            
            DraftDataManager dataManager = gui.getDataManager();
            
            draftIO.saveDraft(dataManager.getDraft());
            saved = true;
        } else if (selection.equals(YesNoCancelDialog.CANCEL)) {
            return false;
        }

        return true;
    }

    private void promptToOpen(WDK_GUI gui) {
        FileChooser draftFileChooser = new FileChooser();
        draftFileChooser.setInitialDirectory(new File("./data/drafts/"));
        File selectedFile = draftFileChooser.showOpenDialog(gui.getWindow());
        
        if (selectedFile != null) {
            try {
                Draft draftToLoad = gui.getDataManager().getDraft();
                draftIO.loadDraft(draftToLoad, selectedFile.getAbsolutePath());
                gui.reloadDraft(draftToLoad);
                saved = true;
                gui.updateToolbarControls(saved);
            } catch (IOException e) {
                System.out.println("COULDN'T PROMT TO OPEN");
//                ErrorHandler eH = ErrorHandler.getErrorHandler();
//                eH.handleLoadDraftError();
            }
        }
    }
    
    public void markFileAsNotSaved() {
        saved = false;
    }
    
    public boolean isSaved() {
        return saved;
    }
}
