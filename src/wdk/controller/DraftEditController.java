package wdk.controller;

import java.util.ArrayList;
import java.util.Collections;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.FantasyTeam;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.gui.MessageDialog;

public class DraftEditController {

    private boolean enabled;
    private DraftDataManager dataManager;
    private MessageDialog messageDialog;
    private Draft draft;
    private ObservableList<FantasyTeam> fantasyTeams;
    private ObservableList<Player> playerPool;
    private ObservableList<Pitcher> pitchers;
    private ObservableList<Hitter> hitters;
    private int hittersNeeded = 0;
    private int pitchersNeeded = 0;
    private int totalWealth = 0;
    private double medianHitterSalary = 0;
    private double medianPitcherSalary = 0;
    private FantasyTeam dummy;
    private Player dumber;
    //private int wealth;

    public DraftEditController(DraftDataManager dataManager, MessageDialog messageDialog) {
        enabled = true;
        this.dataManager = dataManager;
        this.draft = dataManager.getDraft();
        this.messageDialog = messageDialog;
        this.fantasyTeams = this.draft.getFantasyTeams();
        this.playerPool = this.draft.getPlayerPool();
        hitters = FXCollections.observableArrayList();
        pitchers = FXCollections.observableArrayList();
        dummy = new FantasyTeam();
        //initChangeListeners();
    }

    // LIST CHANGE LISTENER
    public void initChangeListeners() {
        fantasyTeams.addListener(new ListChangeListener<FantasyTeam>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends FantasyTeam> change) {
                while (change.next()) {
                    if (change.wasAdded()) {
                        System.out.println("FANTASY TEAM CHANGE________");
                        rankTeams();
                        rankPlayers();
                    }
                    if (change.wasRemoved()) {
                        rankTeams();
                        rankPlayers();
                    }
                }
            }
        });

        playerPool.addListener(new ListChangeListener<Player>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends Player> change) {
                while (change.next()) {
                    System.out.println("PLAYER POOL CHANGE_________");
                    if (change.wasAdded()) {
                        rankTeams();
                        rankPlayers();
                    }
                    if (change.wasRemoved()) {
                        rankTeams();
                        rankPlayers();
                    }
                }
            }
        });
    }

    public void rankTeams() {
        int wealth = 0;
        int hittersNeeded = 0;
        int pitchersNeeded = 0;
        // RESETS POINTS
        for (int i = 0; i < fantasyTeams.size(); i++) {
            fantasyTeams.get(i).resetPoints();
        }
        setTotalWealth(0);
        setHittersNeeded(0);
        setPitchersNeeded(0);
        for (FantasyTeam ft : fantasyTeams) {
            wealth += ft.getWallet();
            hittersNeeded += ft.getHittersNeeded();
            pitchersNeeded += ft.getPitchersNeeded();
        }
        setTotalWealth(wealth);
        setHittersNeeded(hittersNeeded);
        setPitchersNeeded(pitchersNeeded);

        // SORTS TEAMS BY EACH OF THE 10 CATEGORIES AND THEN ASSIGNS POINTS BASED ON
        // WHAT POSITION OF THE SORTED LIST THEY ARE IN 
        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTeamRuns().compareTo(t2.getTeamRuns()));
        collectTeamPoints();
        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTeamHomeruns().compareTo(t2.getTeamHomeruns()));
        collectTeamPoints();
        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTeamRBI().compareTo(t2.getTeamRBI()));
        collectTeamPoints();
        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTeamStolenBases().compareTo(t2.getTeamStolenBases()));
        collectTeamPoints();
        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTeamBattingAverage().compareTo(t2.getTeamBattingAverage()));
        collectTeamPoints();
        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTeamWins().compareTo(t2.getTeamWins()));
        collectTeamPoints();
        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTeamSaves().compareTo(t2.getTeamSaves()));
        collectTeamPoints();
        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTeamStrikeouts().compareTo(t2.getTeamStrikeouts()));
        collectTeamPoints();
        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTeamERA().compareTo(t2.getTeamERA()));
        collectTeamPoints();
        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTeamWHIP().compareTo(t2.getTeamWHIP()));
        collectTeamPoints();

        Collections.sort(fantasyTeams, (FantasyTeam t1, FantasyTeam t2) -> t1.getTotalPoints().compareTo(t2.getTotalPoints()));
        draft.getFantasyTeams().add(dummy);
        draft.getFantasyTeams().remove(dummy);
    }

    public void rankPlayers() {
        if (!fantasyTeams.isEmpty()) {
            // HITTERS
            hitters.clear();
            pitchers.clear();
            for (Player p : playerPool) {
                if (p instanceof Hitter) {
                    hitters.add((Hitter) p);
                    p.resetRank();
                } else if (p instanceof Pitcher) {
                    pitchers.add((Pitcher) p);
                    p.resetRank();
                }
            }
            Collections.sort(hitters, (Hitter h2, Hitter h1) -> h1.getRuns().compareTo(h2.getRuns()));
            collectHitterPoints();
            Collections.sort(hitters, (Hitter t2, Hitter t1) -> t1.getHomeRuns().compareTo(t2.getHomeRuns()));
            collectHitterPoints();
            Collections.sort(hitters, (Hitter t2, Hitter t1) -> t1.getRbi().compareTo(t2.getRbi()));
            collectHitterPoints();
            Collections.sort(hitters, (Hitter t2, Hitter t1) -> t1.getStolenBases().compareTo(t2.getStolenBases()));
            collectHitterPoints();
            Collections.sort(hitters, (Hitter t2, Hitter t1) -> t1.getBa().compareTo(t2.getBa()));
            collectHitterPoints();

            // PITCHERS
            Collections.sort(pitchers, (Pitcher t2, Pitcher t1) -> t1.getWins().compareTo(t2.getWins()));
            collectPitcherPoints();
            Collections.sort(pitchers, (Pitcher t2, Pitcher t1) -> t1.getSaves().compareTo(t2.getSaves()));
            collectPitcherPoints();
            Collections.sort(pitchers, (Pitcher t2, Pitcher t1) -> t1.getStrikeOuts().compareTo(t2.getStrikeOuts()));
            collectPitcherPoints();
            Collections.sort(pitchers, (Pitcher t2, Pitcher t1) -> t1.getEra().compareTo(t2.getEra()));
            collectPitcherPoints();
            Collections.sort(pitchers, (Pitcher t2, Pitcher t1) -> t1.getWhip().compareTo(t2.getWhip()));
            collectPitcherPoints();

            Collections.sort(hitters, (Hitter h1, Hitter h2) -> h1.getRank().compareTo(h2.getRank()));
            Collections.sort(pitchers, (Pitcher p1, Pitcher p2) -> p1.getRank().compareTo(p2.getRank()));
            assignHitterValues();
            assignPitcherValues();
            Collections.sort(hitters, (Hitter h1, Hitter h2) -> h1.getRank().compareTo(h2.getRank()));
            Collections.sort(pitchers, (Pitcher p1, Pitcher p2) -> p1.getRank().compareTo(p2.getRank()));
            draft.getPlayerPool().add(dumber);
             draft.getPlayerPool().remove(dumber);
        }
    }

    public void collectTeamPoints() {
        for (int i = 0; i < fantasyTeams.size(); i++) {
            fantasyTeams.get(i).setTotalPoints(i + 1);
        }
    }

    public void collectPitcherPoints() {
        for (int i = 0; i < pitchers.size(); i++) {
            int rank = pitchers.get(i).getRank();
            pitchers.get(i).setRank(i + 1);

        }
    }

    public void collectHitterPoints() {
        for (int i = 0; i < hitters.size(); i++) {
            hitters.get(i).setRank(i + 1);
        }
    }

    public void assignHitterValues() {
        double ratio = 0;
        if (hittersNeeded == 0) {
            ratio = 0;
        } else {
            ratio = totalWealth / (2 * hittersNeeded);
        }
        for (Hitter h : hitters) {
            h.setEstimatedValue((int) Math.round(ratio * (hittersNeeded * (2.0 / h.getRank()))));
            if (h.getEstimatedValue() < 1) {
                h.setEstimatedValue(1);
            }
        }
    }

    public void assignPitcherValues() {
        double ratio = 0;
        if (pitchersNeeded == 0) {
            ratio = 0;
        } else {
            ratio = totalWealth / (2 * pitchersNeeded);
        }
        for (Pitcher p : pitchers) {
            p.setEstimatedValue((int) Math.round(ratio * (pitchersNeeded * (2.0 / p.getRank()))));
            if (p.getEstimatedValue() < 1) {
                p.setEstimatedValue(1);
            }
        }

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void enable(boolean enableSetting) {
        enabled = enableSetting;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public DraftDataManager getDataManager() {
        return dataManager;
    }

    public void setDataManager(DraftDataManager dataManager) {
        this.dataManager = dataManager;
    }

    public MessageDialog getMessageDialog() {
        return messageDialog;
    }

    public void setMessageDialog(MessageDialog messageDialog) {
        this.messageDialog = messageDialog;
    }

    public Draft getDraft() {
        return draft;
    }

    public void setDraft(Draft draft) {
        this.draft = draft;
    }

    public ObservableList<FantasyTeam> getFantasyTeams() {
        return fantasyTeams;
    }

    public void setFantasyTeams(ObservableList<FantasyTeam> fantasyTeams) {
        this.fantasyTeams = fantasyTeams;
    }

    public ObservableList<Player> getPlayerPool() {
        return playerPool;
    }

    public void setPlayerPool(ObservableList<Player> playerPool) {
        this.playerPool = playerPool;
    }

    public int getHittersNeeded() {
        return hittersNeeded;
    }

    public void setHittersNeeded(int hittersNeeded) {
        this.hittersNeeded = hittersNeeded;
    }

    public int getPitchersNeeded() {
        return pitchersNeeded;
    }

    public void setPitchersNeeded(int pitchersNeeded) {
        this.pitchersNeeded = pitchersNeeded;
    }

    public int getTotalWealth() {
        return totalWealth;
    }

    public void setTotalWealth(int totalWealth) {
        this.totalWealth = totalWealth;
    }

}
