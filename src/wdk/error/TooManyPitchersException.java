package wdk.error;

import wdk.gui.MessageDialog;

public class TooManyPitchersException extends Exception {

    public TooManyPitchersException() {
        super();
    }

    public TooManyPitchersException(String message) {
        super(message);
    }

}
