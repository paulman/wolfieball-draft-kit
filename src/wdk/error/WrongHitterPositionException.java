package wdk.error;

public class WrongHitterPositionException extends Exception {

    public WrongHitterPositionException() {
        super();
    }

    public WrongHitterPositionException(String message) {
        super(message);
    }
}
