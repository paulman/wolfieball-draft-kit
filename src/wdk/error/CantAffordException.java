package wdk.error;

public class CantAffordException extends Exception {

    public CantAffordException() {
        super();
    }

    public CantAffordException(String message) {
        super(message);
    }
}