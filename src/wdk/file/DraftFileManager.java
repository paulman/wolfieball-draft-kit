
package wdk.file;

import wdk.data.Draft;
import wdk.data.Player;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import java.io.IOException;
import java.util.ArrayList;

public interface DraftFileManager {
    public void saveDraft(Draft draftToSave) throws IOException;
    public void loadDraft(Draft draftToLoad, String draftPath) throws IOException;
    
    public ArrayList<Hitter> loadHitters(String jsonFilePath) throws IOException;
    public ArrayList<Pitcher> loadPitchers(String jsonFilePath) throws IOException;
}
