package wdk.file;

import static wdk.WDK_INITConstants.PATH_DRAFTS;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.FantasyTeam;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonValue;
import wdk.data.Draft;
import wdk.data.Player;

public class JsonDraftFileManager implements DraftFileManager {

    String JSON_HITTERS = "Hitters";
    String JSON_PITCHERS = "Pitchers";

    // ALL PLAYER ATTRIBUTES
    String JSON_TEAM = "TEAM";
    String JSON_LAST_NAME = "LAST_NAME";
    String JSON_FIRST_NAME = "FIRST_NAME";
    String JSON_HITS = "H";
    String JSON_NOTES = "NOTES";
    String JSON_YOB = "YEAR_OF_BIRTH";
    String JSON_NOB = "NATION_OF_BIRTH";

    // HITTER ATTRIBUTES
    String JSON_QUALIFIED_POSITIONS = "QP";
    String JSON_AT_BATS = "AB";
    String JSON_RUNS = "R";
    String JSON_HOMERUNS = "HR";
    String JSON_RBI = "RBI";
    String JSON_STOLEN_BASES = "SB";

    // PITCHER ATTRIBUTES
    String JSON_INNINGS_PITCHED = "IP";
    String JSON_EARNED_RUNS = "ER"; // ERA = ER/IP;
    String JSON_WINS = "W";
    String JSON_SAVES = "SV";
    String JSON_STRIKEOUTS = "K";
    String JSON_BASES_ON_BALLS = "BB";

    // STUFF
    String JSON_EXT = ".json";
    String SLASH = "/";
    String PATH_DRAFTS = "./data/drafts/";

    @Override
    public void saveDraft(Draft draftToSave) throws IOException {
        String draftName = "" + draftToSave.getName();
        String jsonFilePath = PATH_DRAFTS + draftName + JSON_EXT;

        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);

        JsonArray playerPoolArray = makePlayerPoolArray(draftToSave.getPlayerPool());

        JsonArray fanTeamsJsonArray = makeFantasyTeamArray(draftToSave.getFantasyTeams());

        JsonArray draftOrder = makeDraftOrderArray(draftToSave.getDraftOrder());

        JsonObject draftJsonObject = Json.createObjectBuilder()
                .add("DRAFT_NAME", draftToSave.getName())
                .add("DRAFT_ORDER", draftOrder)
                .add("PLAYER_POOL", playerPoolArray)
                .add("FANTASY_TEAMS", fanTeamsJsonArray).build();

        jsonWriter.writeObject(draftJsonObject);

        // DRAFT JSON ARRAY
        // FANTASY TEAM JSON ARRAY 
        // PLAYER JSON ARRAY
        // DRAFT ORDER 
        // PLAYER POOL JSON ARRAY
        // PLAYER JSON ARRAY
    }

    @Override
    public void loadDraft(Draft draftToLoad, String draftPath) throws IOException {
        JsonObject json = loadJSONFile(draftPath);
        draftToLoad.setName(json.getString("DRAFT_NAME"));

        // LOAD  FANTASY TEAMS 
        draftToLoad.getFantasyTeams().clear();
        JsonArray fanTeamsJson = json.getJsonArray("FANTASY_TEAMS");

        for (int i = 0; i < fanTeamsJson.size(); i++) {
            if (!fanTeamsJson.isNull(i)) {
                JsonObject teamObj = fanTeamsJson.getJsonObject(i);
                FantasyTeam tempTeam = new FantasyTeam();
                tempTeam.setTeamName(teamObj.getString("TEAM_NAME"));
                tempTeam.setOwnerName(teamObj.getString("OWNER_NAME"));
                tempTeam.setWallet(Integer.valueOf(teamObj.getString("WALLET")));
                tempTeam.setPositionsNeeded(FXCollections.observableArrayList(new ArrayList<String>(Arrays.asList(teamObj.getString("POSITIONS_NEEDED").split("_")))));
                tempTeam.loadTotalPoints(Integer.valueOf(teamObj.getString("TOTAL_POINTS")));
                JsonArray roster = teamObj.getJsonArray("ROSTER");
                for (int j = 0; j < roster.size(); j++) {
                    if (!roster.isNull(j)) {
                        JsonObject playerObj = roster.getJsonObject(j);
                        Player tempPlayer = new Player();
                        tempPlayer = loadPlayer(playerObj);
                        tempPlayer.setFreeAgent(false);
                        tempTeam.getRoster().add(tempPlayer);
                    }
                }
                JsonArray taxiSquad = teamObj.getJsonArray("TAXI_SQUAD");
                for (int x = 0; x < taxiSquad.size(); x++) {
                    if (!taxiSquad.isNull(x)) {
                        JsonObject playerObj = taxiSquad.getJsonObject(x);
                        Player tempPlayer = new Player();
                        tempPlayer = loadPlayer(playerObj);
                        tempPlayer.setFreeAgent(false);
                        tempTeam.getTaxiSquad().add(tempPlayer);
                    }
                }
                draftToLoad.getFantasyTeams().add(tempTeam);
            }
        }

        // LOAD PLAYER POOL 
        JsonArray playerPoolJson = json.getJsonArray("PLAYER_POOL");
        draftToLoad.getPlayerPool().clear();
        for (int i = 0; i < playerPoolJson.size(); i++) {
            if (!playerPoolJson.isNull(i)) {
                JsonObject playerObj = playerPoolJson.getJsonObject(i);
                Player player = loadPlayer(playerObj);
                player.setFreeAgent(true);

                draftToLoad.getPlayerPool().add(player);
            }
        }

        // DRAFT ORDER 
        draftToLoad.getDraftOrder().clear();
        JsonArray draftOrderJson = json.getJsonArray("DRAFT_ORDER");
        for (int i = 0; i < draftOrderJson.size(); i++) {
            if (!draftOrderJson.isNull(i)) {
                JsonObject playerObj = draftOrderJson.getJsonObject(i);

                draftToLoad.getDraftOrder().add(loadPlayer(playerObj));
            }
        }

    }

    private Player loadPlayer(JsonObject playerObj) {
        String pos = playerObj.getString("QP");
        //Player tempPlayer;
        if (pos.equals("P")) {
            Pitcher tempPlayer = new Pitcher();
            tempPlayer.setProTeam(playerObj.getString("TEAM"));
            tempPlayer.setName(playerObj.getString("LAST_NAME"),
                    (playerObj.getString("FIRST_NAME")));
            tempPlayer.setPositions("P");
            tempPlayer.setQp("P");
            tempPlayer.setFixedPosition("P");
            tempPlayer.setNotes(playerObj.getString("NOTES"));
            tempPlayer.setHits(Integer.valueOf(playerObj.getString(JSON_HITS)));
            tempPlayer.setProTeam(playerObj.getString(JSON_TEAM));
            tempPlayer.setYOB(Integer.valueOf(playerObj.getString(JSON_YOB)));
            tempPlayer.setNOB(playerObj.getString(JSON_NOB));
            tempPlayer.setBaseOnBalls(Integer.valueOf(playerObj.getString(JSON_BASES_ON_BALLS)));
            tempPlayer.setInningsPitched(Double.parseDouble(playerObj.getString(JSON_INNINGS_PITCHED)));
            tempPlayer.setWins(Integer.valueOf(playerObj.getString(JSON_WINS)));
            if (playerObj.getString("FIRST_NAME").equals("Yimi")) {
            System.out.println(")))))))))))))))))))))))))))))))))))))))))))))))))))))))))\n" + Integer.valueOf(playerObj.getString(JSON_WINS)));
        }
            
            tempPlayer.setSaves(Integer.valueOf(playerObj.getString(JSON_SAVES)));
            tempPlayer.setStrikeOuts(Integer.valueOf(playerObj.getString(JSON_STRIKEOUTS)));
            tempPlayer.setEarnedRuns(Integer.valueOf(playerObj.getString(JSON_EARNED_RUNS)));
            tempPlayer.setContract(playerObj.getString("CONTRACT"));
            tempPlayer.setSalary(Integer.valueOf(playerObj.getString("SALARY")));
            tempPlayer.loadEstimatedValue(Integer.valueOf(playerObj.getString("ESTIMATED_VALUE")));
            tempPlayer.setFanTeamName(playerObj.getString("FAN_TEAM"));
           

            return tempPlayer;
        } else {
            Hitter tempPlayer = new Hitter();
            tempPlayer.setName(playerObj.getString(JSON_LAST_NAME),
                    playerObj.getString(JSON_FIRST_NAME));
            tempPlayer.setFixedPosition(playerObj.getString("FIXED_POSITION"));
            tempPlayer.setNotes(playerObj.getString("NOTES"));
            tempPlayer.setProTeam(playerObj.getString(JSON_TEAM));
            tempPlayer.setYOB(Integer.valueOf(playerObj.getString(JSON_YOB)));
            tempPlayer.setNOB(playerObj.getString(JSON_NOB));
            tempPlayer.setHits(Integer.valueOf(playerObj.getString(JSON_HITS)));
            tempPlayer.setPositions(playerObj.getString(JSON_QUALIFIED_POSITIONS) + "_U");
            tempPlayer.setQp(playerObj.getString(JSON_QUALIFIED_POSITIONS));
            tempPlayer.setAtBats(Integer.valueOf(playerObj.getString(JSON_AT_BATS)));
            tempPlayer.setRuns(Integer.valueOf(playerObj.getString(JSON_RUNS)));
            tempPlayer.setHomeRuns(Integer.valueOf(playerObj.getString(JSON_HOMERUNS)));
            tempPlayer.setRbi(Integer.valueOf(playerObj.getString(JSON_RBI)));
            tempPlayer.setStolenBases(Integer.valueOf(playerObj.getString(JSON_RBI)));
            tempPlayer.setContract(playerObj.getString("CONTRACT"));
            tempPlayer.setSalary(Integer.valueOf(playerObj.getString("SALARY")));
            tempPlayer.loadEstimatedValue(Integer.valueOf(playerObj.getString("ESTIMATED_VALUE")));
            tempPlayer.setFanTeamName(playerObj.getString("FAN_TEAM"));
           

            return tempPlayer;
        }

    }

    @Override
    public ArrayList<Hitter> loadHitters(String jsonFilePath) throws IOException {
        JsonObject jsonObj = loadJSONFile(jsonFilePath);

        ArrayList<Hitter> hitters = new ArrayList<Hitter>();
        JsonArray jsonArr = jsonObj.getJsonArray(JSON_HITTERS);

        String position;
        for (int i = 0; i < jsonArr.size(); i++) {
            JsonObject tempObj = jsonArr.getJsonObject(i);
            Hitter tempHitter = new Hitter();
            tempHitter.setName(tempObj.getString(JSON_LAST_NAME),
                    tempObj.getString(JSON_FIRST_NAME));
            tempHitter.setNotes(tempObj.getString("NOTES"));
            tempHitter.setProTeam(tempObj.getString(JSON_TEAM));
            tempHitter.setYOB(Integer.valueOf(tempObj.getString(JSON_YOB)));
            tempHitter.setNOB(tempObj.getString(JSON_NOB));
            tempHitter.setHits(Integer.valueOf(tempObj.getString(JSON_HITS)));
            tempHitter.setPositions(tempObj.getString(JSON_QUALIFIED_POSITIONS) + "_U");
            tempHitter.setQp(tempObj.getString(JSON_QUALIFIED_POSITIONS));
            tempHitter.setAtBats(Integer.valueOf(tempObj.getString(JSON_AT_BATS)));
            tempHitter.setRuns(Integer.valueOf(tempObj.getString(JSON_RUNS)));
            tempHitter.setHomeRuns(Integer.valueOf(tempObj.getString(JSON_HOMERUNS)));
            tempHitter.setRbi(Integer.valueOf(tempObj.getString(JSON_RBI)));
            tempHitter.setStolenBases(Integer.valueOf(tempObj.getString(JSON_STOLEN_BASES)));
            try {
                tempHitter.setContract(tempObj.getString("CONTRACT"));
                tempHitter.setEstimatedValue(Integer.valueOf(tempObj.getString("ESTIMATED_VALUE")));
                tempHitter.setFanTeamName(tempObj.getString("FAN_TEAM"));
            } catch (NullPointerException e) {
                // SIMPLY DON'T LOAD THEM 
            }

            hitters.add(tempHitter);
        }

        return hitters;
    }

    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {

        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    private ArrayList<Hitter> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<Hitter> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add((Hitter) jsV);
        }
        return items;
    }

    @Override
    public ArrayList<Pitcher> loadPitchers(String jsonFilePath) throws IOException {
        JsonObject jsonObj = loadJSONFile(jsonFilePath);

        ArrayList<Pitcher> pitchers = new ArrayList<Pitcher>();
        JsonArray jsonArr = jsonObj.getJsonArray(JSON_PITCHERS);

        for (int i = 0; i < jsonArr.size(); i++) {
            JsonObject tempObj = jsonArr.getJsonObject(i);
            Pitcher tempPitcher = new Pitcher();
            tempPitcher.setName(tempObj.getString(JSON_LAST_NAME),
                    tempObj.getString(JSON_FIRST_NAME));

            tempPitcher.setPositions("P");
            tempPitcher.setQp("P");
            tempPitcher.setNotes(tempObj.getString("NOTES"));
            tempPitcher.setHits(Integer.valueOf(tempObj.getString(JSON_HITS)));
            tempPitcher.setProTeam(tempObj.getString(JSON_TEAM));
            tempPitcher.setYOB(Integer.valueOf(tempObj.getString(JSON_YOB)));
            tempPitcher.setNOB(tempObj.getString(JSON_NOB));
            tempPitcher.setBaseOnBalls(Integer.valueOf(tempObj.getString(JSON_BASES_ON_BALLS)));
            tempPitcher.setInningsPitched(Double.parseDouble(tempObj.getString(JSON_INNINGS_PITCHED)));
            tempPitcher.setWins(Integer.valueOf(tempObj.getString(JSON_WINS)));
            tempPitcher.setSaves(Integer.valueOf(tempObj.getString(JSON_SAVES)));
            tempPitcher.setStrikeOuts(Integer.valueOf(tempObj.getString(JSON_STRIKEOUTS)));
            tempPitcher.setEarnedRuns(Integer.valueOf(tempObj.getString(JSON_EARNED_RUNS)));
            

            try {
                tempPitcher.setContract(tempObj.getString("CONTRACT"));
                tempPitcher.setEstimatedValue(Integer.valueOf(tempObj.getString("ESTIMATED_VALUE")));
                tempPitcher.setFanTeamName(tempObj.getString("FAN_TEAM"));
            } catch (NullPointerException e) {

            }

            pitchers.add(tempPitcher);
        }

        return pitchers;
    }

    private JsonArray makeFantasyTeamArray(ObservableList<FantasyTeam> fanTeams) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (FantasyTeam team : fanTeams) {
            jsb.add(makeFanTeamObject(team));
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonObject makeFanTeamObject(FantasyTeam fanTeam) {
        JsonObject jso = Json.createObjectBuilder()
                .add("TEAM_NAME", fanTeam.getTeamName())
                .add("OWNER_NAME", fanTeam.getOwnerName())
                .add("POSITIONS_NEEDED", join(fanTeam.getPositionsNeeded()))
                .add("ROSTER", makeTeamRosterArray(fanTeam.getRoster()))
                .add("TAXI_SQUAD", makeTeamRosterArray(fanTeam.getTaxiSquad()))
                .add("WALLET", fanTeam.getWallet() + "")
                .add("TOTAL_POINTS", fanTeam.getTotalPoints() + "")
                .build();
        return jso;
    }

    private String join(ObservableList<String> list) {
        String pos = "";
        for (int i = 0; i < list.size(); i++) {
            pos += list.get(i);
            if (i != list.size() - 1) {
                pos += "_";
            }
        }
        return pos;
    }

    private JsonArray stringJsonArray(ArrayList<String> list) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (String s : list) {
            jsb.add("");
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonArray makeTeamRosterArray(ObservableList<Player> roster) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Player p : roster) {
            jsb.add(makePlayerJsonObject(p));
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonArray makePlayerPoolArray(ObservableList<Player> playerPool) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Player p : playerPool) {
            jsb.add(makePlayerJsonObject(p));
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonObject makePlayerJsonObject(Player p) {
        if (p instanceof Hitter) {
            JsonObject jso = Json.createObjectBuilder()
                    .add("TEAM", p.getProTeam())
                    .add("LAST_NAME", p.getLName())
                    .add("FIRST_NAME", p.getFName())
                    .add("QP", p.getQp())
                    .add("AB", ((Hitter) p).getAtBats() + "")
                    .add("R", ((Hitter) p).getRuns() + "")
                    .add("H", ((Hitter) p).getHits() + "")
                    .add("HR", ((Hitter) p).getHomeRuns() + "")
                    .add("RBI", ((Hitter) p).getRbi() + "")
                    .add("SB", ((Hitter) p).getStolenBases() + "")
                    .add("NOTES", p.getNotes())
                    .add("YEAR_OF_BIRTH", p.getYOB() + "")
                    .add("NATION_OF_BIRTH", p.getNOB())
                    .add("CONTRACT", p.getContract())
                    .add("SALARY", p.getSalary() + "")
                    .add("ESTIMATED_VALUE", p.getEstimatedValue() + "")
                    .add("FIXED_POSITION", p.getFixedPosition())
                    .add("FAN_TEAM", p.getFanTeamName())
                    .add("FREE_AGENT", String.valueOf(p.isFreeAgent()))
                    .build();
            return jso;
        } else { // IS A PITCHER 
            JsonObject jso = Json.createObjectBuilder()
                    .add("TEAM", p.getProTeam())
                    .add("LAST_NAME", p.getLName())
                    .add("FIRST_NAME", p.getFName())
                    .add("QP", "P")
                    .add("IP", ((Pitcher) p).getInningsPitched() + "")
                    .add("ER", ((Pitcher) p).getEarnedRuns() + "")
                    .add("H", ((Pitcher) p).getHits() + "")
                    .add("W", ((Pitcher) p).getWins() + "")
                    .add("SV", ((Pitcher) p).getSaves() + "")
                    .add("BB", ((Pitcher) p).getBaseOnBalls() + "")
                    .add("K", ((Pitcher) p).getStrikeOuts() + "")
                    .add("NOTES", p.getNotes())
                    .add("YEAR_OF_BIRTH", p.getYOB() + "")
                    .add("NATION_OF_BIRTH", p.getNOB())
                    .add("CONTRACT", p.getContract())
                    .add("SALARY", p.getSalary() + "")
                    .add("ESTIMATED_VALUE", p.getEstimatedValue() + "")
                    .add("FIXED_POSITION", p.getFixedPosition())
                    .add("FAN_TEAM", p.getFanTeamName())
                    .add("FREE_AGENT", String.valueOf(p.isFreeAgent()))
                    .build();
            return jso;
        }
    }

    private JsonArray makeDraftOrderArray(ObservableList<Player> draftOrder) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (int i = 0; i < draftOrder.size(); i++) {
            jsb.add(makePlayerJsonObject(draftOrder.get(i)));
        }
        JsonArray jA = jsb.build();
        return jA;
    }

}
