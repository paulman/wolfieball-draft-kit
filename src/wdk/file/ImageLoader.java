package wdk.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import wdk.WDK_INITConstants.*;
import static wdk.WDK_INITConstants.*;
import wdk.data.Player;
//import static wdk.WDK_INITConstants.PATH_IMAGES;

public class ImageLoader {

    private static final String flagPath = "file:./images/FLAGS/";// + "FLAGS/";
    private static final String playerPath = "file:./images/PLAYERS/";//PATH_IMAGES + "PLAYERS/";
    private static final String png = ".png";
    private static final String jpg = ".jpg";

    private HashMap<String, Image> playerImages;

    private HashMap<String, Image> flags;

    private ArrayList<Player> players;

    public ImageLoader(ArrayList<Player> players) {
        this.players = players;
        playerImages = new HashMap<String, Image>();
        flags = new HashMap<String, Image>();
        initFlags();
        initPlayerImages();
        initUnknownImages();
    }

    private void initUnknownImages() {
        playerImages.put("unknown", new Image(playerPath + "player.jpg"));
        flags.put("unknown", new Image(flagPath + "earth_flag.png"));
    }

    private void initFlags() {
        for (Player p : players) {
            String nation = p.getNOB();
            if (!flags.containsKey(nation)) {
                flags.put(nation, new Image(flagPath + nation + png));
            }
        }

    }

    private void initPlayerImages() {
        String playerName;
        Image playerImage;
        for (Player p : players) {
            playerName = p.getLName() + p.getFName();
            playerImage = new Image(playerPath + playerName + jpg);
            if (playerImage.getWidth() > 0) {
                playerImages.put(playerName, playerImage);
            }
        }
    }

    public HashMap<String, Image> getPlayerImages() {
        return playerImages;
    }

    public void setPlayerImages(HashMap<String, Image> playerImages) {
        this.playerImages = playerImages;
    }

    public HashMap<String, Image> getFlags() {
        return flags;
    }

    public void setFlags(HashMap<String, Image> flags) {
        this.flags = flags;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

}
