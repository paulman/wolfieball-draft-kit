/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

public class Hitter extends Player{
    private int atBats=0;
    private int runs=0;
    private int hits=0;
    private int homeRuns=0;
    private int rbi=0;
    private int stolenBases=0;
    private double ba=0;

    
    public Hitter() {
        
    }
    
    public Double getBa() {
        if (atBats != 0) {
            ba = ((double)hits / atBats);
            ba *= 1000;
            ba = Math.round(ba);
            ba /= 1000;
            return ba;
        }
        return 0.0;
    } 


    public Integer getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public Integer getAtBats() {
        return atBats;
    }

    public void setAtBats(int atBats) {
        this.atBats = atBats;
    }

    public Integer getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public Integer getHomeRuns() {
        return homeRuns;
    }

    public void setHomeRuns(int homeRuns) {
        this.homeRuns = homeRuns;
    }

    public Integer getRbi() {
        return rbi;
    }

    public void setRbi(int rbi) {
        this.rbi = rbi;
    }

    public Integer getStolenBases() {
        return stolenBases;
    }

    public void setStolenBases(int stolenBases) {
        this.stolenBases = stolenBases;
    }  
    
}
