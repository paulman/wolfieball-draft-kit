package wdk.data;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Draft {
    ObservableList<Player> draftOrder;
    ObservableList<Player> playerPool;
    ObservableList<FantasyTeam> fantasyTeams;
    ObservableList<Player> allPlayers;
    String name;
    
    public Draft() {
        fantasyTeams = FXCollections.observableArrayList();
        draftOrder = FXCollections.observableArrayList();
    }
    
    public ObservableList<Player> getPlayerPool() {
        return playerPool;
    }

    public ObservableList<FantasyTeam> getFantasyTeams() {
        return fantasyTeams;
    }

    public void setPlayerPool(ArrayList<Player> players) {
        this.playerPool = FXCollections.observableArrayList(players);
        this.allPlayers = FXCollections.observableArrayList(players);
    }
    
    public void setFantasyTeams(ObservableList<FantasyTeam> fantasyTeams) {
        this.fantasyTeams = fantasyTeams;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String toString() {
        System.out.println(playerPool.get(0).getName());
        return "I AM NOT NULL";
    }

    public ObservableList<Player> getDraftOrder() {
        return draftOrder;
    }

    public void setDraftOrder(ObservableList<Player> draftOrder) {
        this.draftOrder = draftOrder;
    }

    public ObservableList<Player> getAllPlayers() {
        return allPlayers;
    }

    public void setAllPlayers(ObservableList<Player> allPlayers) {
        this.allPlayers = allPlayers;
    }    
}
