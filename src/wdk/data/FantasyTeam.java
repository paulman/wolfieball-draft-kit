package wdk.data;

import java.util.ArrayList;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wdk.error.CantAffordException;
import wdk.error.TooManyPitchersException;
import wdk.error.WrongHitterPositionException;

public class FantasyTeam {

    private String teamName = "";
    private String ownerName = "";
    private ObservableList<Pitcher> pitchers;
    private ObservableList<Hitter> hitters;
    private ObservableList<String> positionsNeeded;
    private ObservableList<Player> draftOrder;
    private int wallet = 260;
    private int moneyLeftPerPlayer = 0;
    private int totalPoints = 0;

    private ObservableList<Player> roster;
    private ObservableList<Player> taxiSquad;

    public FantasyTeam() {
        positionsNeeded = FXCollections.observableArrayList();
        fillPositionsNeeded();
        roster = FXCollections.observableArrayList();
        taxiSquad = FXCollections.observableArrayList();
    }

    public void fillPositionsNeeded() {
        positionsNeeded.add("C");
        positionsNeeded.add("C");
        positionsNeeded.add("1B");
        positionsNeeded.add("3B");
        positionsNeeded.add("CI");
        positionsNeeded.add("2B");
        positionsNeeded.add("SS");
        positionsNeeded.add("MI");
        positionsNeeded.add("OF");
        positionsNeeded.add("OF");
        positionsNeeded.add("OF");
        positionsNeeded.add("OF");
        positionsNeeded.add("OF");
        positionsNeeded.add("U");
        positionsNeeded.add("P");
        positionsNeeded.add("P");
        positionsNeeded.add("P");
        positionsNeeded.add("P");
        positionsNeeded.add("P");
        positionsNeeded.add("P");
        positionsNeeded.add("P");
        positionsNeeded.add("P");
        positionsNeeded.add("P");
    }

    public Integer getHittersNeeded() {
        int hitters = 0;
        for (String s : positionsNeeded) {
            if (!s.equals("P")) {
                hitters++;
            }
        }
        return hitters;
    }

    public Integer getPitchersNeeded() {
        int pitchers = 0;
        for (String s : positionsNeeded) {
            if (s.equals("P")) {
                pitchers++;
            }
        }
        return pitchers;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void removePlayer(Player player) {
        draftOrder.remove(player);
        wallet += player.getSalary();
        roster.remove(player);
    }

    public void addPlayer(Player player) throws CantAffordException {
//        if (!affordSalary(player)) {
//            throw new CantAffordException("Player is too expensive");
//        }
        System.out.println("crash here?_4");
        if (player instanceof Hitter) {
            addHitter((Hitter) player);
        }
        if (player instanceof Pitcher) {
            addPitcher((Pitcher) player);
        }
    }

    public boolean isFull() {
        return getPlayersNeeded() == 0;
    }

    public void insertionSort(Player player, ObservableList<Player> list) {
        if (list.isEmpty()) {
            list.add(player);
        } else {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                int equality = player.getPositionEnum().compareTo(list.get(i).getPositionEnum());
                if (i == size - 1 && equality > 0) {
                    list.add(player);
                    break;
                }
                if (equality == 0) {
                    list.add(i, player);
                    break;
                } else if (equality < 0) {
                    list.add(i, player);
                    break;
                } else {
                    continue;

                }
            }

        }
    }

    public void addPitcher(Pitcher pitcher) {
        if (!positionsNeeded.contains("P")) {
            //throw new TooManyPitchersException("Already have 9 pitchers");
        }
        if (pitcher.getContract().equals("X")) {
            if (taxiSquad.size() < 8) {
                insertionSort(pitcher, taxiSquad);
            } else {
                return;
            }

        } else {
            insertionSort(pitcher, roster);
            positionsNeeded.remove("P");
        }
        wallet -= pitcher.getSalary();
    }

    public void addHitter(Hitter hitter) {
        if (!positionsNeeded.contains(hitter.getFixedPosition())) {
            //throw new WrongHitterPositionException("Already have enough: " + hitter.getFixedPosition());
        }
        if (hitter.getContract().equals("X")) {
            if (taxiSquad.size() < 8) {
                insertionSort(hitter, taxiSquad);
            } else {
                return;
            }
        } else {
            insertionSort(hitter, roster);
            positionsNeeded.remove(hitter.getFixedPosition());
        }
        wallet -= hitter.getSalary();
    }

    public int getPlayersNeeded() {
        return 23 - (roster.size());
    }

    public void refresh() {
        Player dummy = new Player();
        dummy.setfName("shit head");
        roster.add(dummy);
        roster.remove(dummy);
        System.out.println("REFRESH");
    }

    public int getMoneyPP() {
        if ((roster.size() + taxiSquad.size()) == 0) {
            return wallet / 23;
        } else {
            if (getPlayersNeeded() == 0) {
                return -1;
            }
            return wallet / (getPlayersNeeded());
        }
    }

    public boolean affordSalary(int salary) {
        if (getPlayersNeeded() + ((8 - taxiSquad.size())) == 0) {
            if (wallet - salary < 1) {
                return false;
            }
        }
        if (getPlayersNeeded() + (8 - taxiSquad.size()) - 1 == 0) {
            return false;
        }
        if ((wallet - salary) / (getPlayersNeeded() + (8 - taxiSquad.size()) - 1) < 1) {
            return false;
        } else {
            return true;
        }
    }

    public Integer getTeamRuns() {
        int runs = 0;
        for (Player p : roster) {
            if (p instanceof Hitter) {
                runs += ((Hitter) p).getRuns();
            }
        }
        return runs;
    }

    public Integer getTeamHomeruns() {
        int homeruns = 0;
        for (Player p : roster) {
            if (p instanceof Hitter) {
                homeruns += ((Hitter) p).getHomeRuns();
            }
        }
        return homeruns;
    }

    public Integer getTeamRBI() {
        int rbi = 0;
        for (Player p : roster) {
            if (p instanceof Hitter) {
                rbi += ((Hitter) p).getRbi();
            }
        }
        return rbi;
    }

    public Integer getTeamStolenBases() {
        int sb = 0;
        for (Player p : roster) {
            if (p instanceof Hitter) {
                sb += ((Hitter) p).getStolenBases();
            }
        }
        return sb;
    }

    public Double getTeamBattingAverage() {
        int ab = 0;
        int hits = 0;
        double temp;
        for (Player p : roster) {
            if (p instanceof Hitter) {
                ab += ((Hitter) p).getAtBats();
                hits += ((Hitter) p).getHits();
            }
        }
        if (ab != 0) {
            temp = (hits * 1.0) / (ab * 1.0);
            temp *= 1000;
            temp = Math.round(temp);
            temp /= 1000;
            return temp;
        }
        return 0.0;
    }

    public Integer getTeamWins() {
        int wins = 0;
        for (Player p : roster) {
            if (p instanceof Pitcher) {
                wins += ((Pitcher) p).getWins();
            }
        }
        return wins;
    }

    public Integer getTeamSaves() {
        int saves = 0;
        for (Player p : roster) {
            if (p instanceof Pitcher) {
                saves += ((Pitcher) p).getSaves();
            }
        }
        return saves;
    }

    public Integer getTeamStrikeouts() {
        int k = 0;
        for (Player p : roster) {
            if (p instanceof Pitcher) {
                k += ((Pitcher) p).getStrikeOuts();
            }
        }
        return k;
    }

    public Double getTeamERA() {
        int er = 0;
        int ip = 0;
        double temp;
        for (Player p : roster) {
            if (p instanceof Pitcher) {
                er += ((Pitcher) p).getEarnedRuns();
                ip += ((Pitcher) p).getInningsPitched();
            }
        }
        if (ip != 0) {
            temp = (er * 9.0) / (ip * 1.0);
            temp *= 100;
            temp = Math.round(temp);
            temp /= 100;
            return temp;
        }
        return 0.0;
    }

    public Double getTeamWHIP() {
        int walks = 0;
        int hits = 0;
        int ip = 0;
        double temp;
        for (Player p : roster) {
            if (p instanceof Pitcher) {
                walks += ((Pitcher) p).getBaseOnBalls();
                hits += ((Pitcher) p).getHits();
                ip += ((Pitcher) p).getInningsPitched();
            }
        }
        if (ip != 0) {
            temp = (walks + hits * 1.0) / (ip * 1.0);
            temp *= 100;
            temp = Math.round(temp);
            temp /= 100;
            return temp;
        }
        return 0.0;
    }

    public int getHighestBid() {
        return wallet - getPlayersNeeded();
    }

    // DUMMY/USELESS METHODS 
    public void setTeam(ObservableList<Player> team) {
        this.roster = team;
    }

    public String toString() {
        return getTeamName();
    }

    public ObservableList<String> getPositionsNeeded() {
        return positionsNeeded;
    }

    public void setPositionsNeeded(ObservableList<String> positionsNeeded) {
        this.positionsNeeded = positionsNeeded;
    }

    public ObservableList<Player> getRoster() {
        return roster;
    }

    public void setRoster(ObservableList<Player> roster) {
        this.roster = roster;
    }

    public ObservableList<Pitcher> getPitchers() {
        return pitchers;
    }

    public void setPitchers(ObservableList<Pitcher> pitchers) {
        this.pitchers = pitchers;
    }

    public ObservableList<Hitter> getHitters() {
        return hitters;
    }

    public void setHitters(ObservableList<Hitter> hitters) {
        this.hitters = hitters;
    }

    public ObservableList<Player> getDraftOrder() {
        return draftOrder;
    }

    public void setDraftOrder(ObservableList<Player> draftOrder) {
        this.draftOrder = draftOrder;
    }

    public int getWallet() {
        return wallet;
    }

    public void setWallet(int wallet) {
        this.wallet = wallet;
    }

    public int getMoneyLeftPerPlayer() {
        return moneyLeftPerPlayer;
    }

    public void setMoneyLeftPerPlayer(int moneyLeftPerPlayer) {
        this.moneyLeftPerPlayer = moneyLeftPerPlayer;
    }

    public Integer getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints += totalPoints;
    }

    public void loadTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public ObservableList<Player> getTaxiSquad() {
        return taxiSquad;
    }

    public void setTaxiSquad(ObservableList<Player> taxiSquad) {
        this.taxiSquad = taxiSquad;
    }

    public void resetPoints() {
        totalPoints = 0;
    }
}
