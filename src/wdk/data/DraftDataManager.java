
package wdk.data;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wdk.file.DraftFileManager;

public class DraftDataManager {
    Draft draft;
    DraftDataView view;
    DraftFileManager fileManager;
    
    
    
    
    public DraftDataManager(DraftDataView initView) { //, ArrayList<Player> players) {
        view = initView;
        
        draft = new Draft();
    }
    
    public void reset(DraftDataView initView) {
       //this.draft.getPlayerPool().clear();
       this.draft.getFantasyTeams().clear();
       this.draft.getDraftOrder().clear();
    }
    
    public Draft getDraft() {
        return draft;
    }
    
    public DraftFileManager getFileManager() {
        return fileManager;
    }
    
    public void reset() {
        // SET ALL draft.VALUES BACK TO DEFAULT
        
        view.reloadDraft(draft);
    }
}
