
package wdk.data;

public class Pitcher extends Player {

    private int baseOnBalls=0;
    private double inningsPitched=0;
    private double era=0;
    private int earnedRuns=0;
    private int wins=0;
    private int saves=0;
    private int strikeOuts=0;
    private int hits=0;
    private double whip=0;

    public Pitcher() {
        //this.wins = new SimpleIntegerProperty();
    }

    public Integer getBaseOnBalls() {
        return baseOnBalls;
    }

    public Integer getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public Double getWhip() {
        if (inningsPitched != 0) {
            whip = (baseOnBalls + hits) / inningsPitched;
            whip *= 100;
            whip = Math.round(whip);
            whip /= 100;
            return whip;
        }
        return 0.0;
    }

    public void setBaseOnBalls(int baseOnBalls) {
        this.baseOnBalls = baseOnBalls;
    }

    public Double getInningsPitched() {
        return inningsPitched;
    }

    public void setInningsPitched(double inningsPitched) {
        this.inningsPitched = inningsPitched;
    }

    public Double getEra() {
        if (inningsPitched != 0) {
            era = earnedRuns / inningsPitched;
            era *= 100;
            era = Math.round(era);
            era /= 100;
            return era;
        }
        return 0.0;
    }

    public Integer getWins() {
        return wins;//.getValue();
    }

    public void setWins(int wins) {
        this.wins = wins;//.set(wins);
    }

    public Integer getSaves() {
        return saves;
    }

    public void setSaves(int saves) {
        this.saves = saves;
    }

    public Integer getStrikeOuts() {
        return strikeOuts;
    }

    public void setStrikeOuts(int strikeOuts) {
        this.strikeOuts = strikeOuts;
    }

    public Integer getEarnedRuns() {
        return earnedRuns;
    }

    public void setEarnedRuns(int earnedRuns) {
        this.earnedRuns = earnedRuns;
    }
}
