package wdk.data;

public interface DraftDataView {
    public void reloadDraft(Draft draftToReload);
}
