package wdk.data;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.String;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wdk.data.Positions;
import static wdk.data.Positions.NONE;

public class Player {

    private String lName = "";
    private String fName = "";
    private String proTeam = "";
    private int YOB =0;
    private String NOB="N/A";
    private String positions="";
    private String notes="N/A";
    private String contract="";
    private int salary=0;
    private int estimatedValue=0;
    private boolean freeAgent = true;
    private Positions fixedPosition=NONE;
    private FantasyTeam fantasyTeam;
    private String fanTeamName = "";
    private String qp = "";
    private String setPosition="";
    private int rank = 0;

    public void setName(String lName, String fName) {
        this.lName = lName;
        this.fName = fName;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public String getFName() {
        return fName;
    }

    public String getLName() {
        return lName;
    }

    public String getName() {
        return lName + ", " + fName;
    }

    public String getIMGName() {
        return lName + fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getProTeam() {
        return proTeam;
    }

    public void setProTeam(String team) {
        this.proTeam = team;
    }

    public int getYOB() {
        return YOB;
    }

    public void setYOB(int YOB) {
        this.YOB = YOB;
    }

    public String getNOB() {
        return NOB;
    }

    public void setNOB(String NOB) {
        this.NOB = NOB;
    }

    public String getPositions() {
        return positions;
    }

    public ObservableList getPositionsList() {
        return FXCollections.observableArrayList(new ArrayList(Arrays.asList(positions.split("\\s*_\\s*"))));
    }

    public void setPositions(String position) {
        if (this instanceof Hitter) {
            if (position.contains("2B") || position.contains("SS")) {
                position += "_MI";
            }
            if (position.contains("1B") || position.contains("3B")) {
                position += "_CI";
            }
        }
        this.positions = position;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(int estimatedValue) {
        this.estimatedValue += estimatedValue;
    }
    
    public void loadEstimatedValue(int estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public boolean isFreeAgent() {
        return freeAgent;
    }

    public void setFreeAgent(boolean freeAgent) {
        this.freeAgent = freeAgent;
    }

    public String getFixedPosition() {
        return fixedPosition.getName();
    }

    public Enum getPositionEnum() {
        return fixedPosition;
    }

    public void setFixedPosition(String fixedPosition) {
        switch (fixedPosition) {
            case "C":
                this.fixedPosition = Positions.CATCHER;
                break;
            case "1B":
                this.fixedPosition = Positions.FIRSTBASE;
                break;
            case "CI":
                this.fixedPosition = Positions.CORNERINFIELD;
                break;
            case "3B":
                this.fixedPosition = Positions.THIRDBASE;
                break;
            case "2B":
                this.fixedPosition = Positions.SECONDBASE;
                break;
            case "MI":
                this.fixedPosition = Positions.MIDDLEINFIELD;
                break;
            case "SS":
                this.fixedPosition = Positions.SHORTSTOP;
                break;
            case "OF":
                this.fixedPosition = Positions.OUTFIELD;
                break;
            case "U":
                this.fixedPosition = Positions.UTILITY;
                break;
            case "P":
                this.fixedPosition = Positions.PITCHER;
                break;
        }
    }

    public FantasyTeam getFantasyTeam() {
        return fantasyTeam;
    }

    public void setFantasyTeam(FantasyTeam fantasyTeam) {
        this.fantasyTeam = fantasyTeam;
    }

 

    public void setFixedPosition(Positions fixedPosition) {
        this.fixedPosition = fixedPosition;
    }

    public String getQp() {
        return qp;
    }

    public void setQp(String qp) {
        this.qp = qp;
    }

    public String getFanTeamName() {
        return fanTeamName;
    }

    public void setFanTeamName(java.lang.String fanTeamName) {
        this.fanTeamName = fanTeamName;
    }
    
    public String getSalaryString() {
        return salary + "";
    }

    public void resetValue() {
        estimatedValue = 0;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank += rank;
    }
    public void resetRank() {
        this.rank = 0;
    }
    
    

}
