package wdk.data;

    public enum Positions {

        NONE(""),
        CATCHER("C"),
        FIRSTBASE("1B"),
        THIRDBASE("3B"),
        CORNERINFIELD("CI"),
        SECONDBASE("2B"),
        SHORTSTOP("SS"),
        MIDDLEINFIELD("MI"),
        OUTFIELD("OF"),
        UTILITY("U"),
        PITCHER("P");

        private final String name;

        private Positions(String p) {
            name = p;
        }

        public String getName() {
            return name;
        }
    }
