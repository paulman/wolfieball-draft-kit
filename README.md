# README #

This was the final project for CSE 219 (Java III) and is a program designed to manage a fantasy baseball league.  The program allowed for the creation of teams and either manual or fully automated drafting of players onto said teams.